import { 
    SET_SCROLL, 
    SET_CURRENT_SCROLL, 
    SHOW_LOCATION_MODAL, 
    SET_LOCATION, 
    SHOW_CATEGORY_MODAL, 
    SET_CATEGORY, 
    SHOW_STORE_MODAL,
    SET_STORE,
    SHOW_SORT_MODAL,
    SET_SORT
} from '../constants';

export const setScroll = (status) => ({
    type: SET_SCROLL,
    payload: {
        status
    }
});

export const setCurrentScroll = (offset) => ({
    type: SET_CURRENT_SCROLL,
    payload: {
        offset
    }
});

export const showLocationModal = (show) => ({
    type: SHOW_LOCATION_MODAL,
    payload: {
        show
    }
});

export const setLocation = (location) => ({
    type: SET_LOCATION,
    payload: {
        location
    }
});

export const showCategoryModal = (show) => ({
    type: SHOW_CATEGORY_MODAL,
    payload: {
        show
    }
});

export const setCategory = (category) => ({
    type: SET_CATEGORY,
    payload: {
        category
    }
});

export const showStoreModal = (show) => ({
    type: SHOW_STORE_MODAL,
    payload: {
        show
    }
});

export const setStore = (store) => ({
    type: SET_STORE,
    payload: {
        store
    }
});

export const showSortModal = (show) => ({
    type: SHOW_SORT_MODAL,
    payload: {
        show
    }
});

export const setSort = (sort) => ({
    type: SET_SORT,
    payload: {
        sort
    }
});
