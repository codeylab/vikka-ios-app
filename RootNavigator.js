import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { addNavigationHelpers } from 'react-navigation';

import { MainNavigator } from './config/routes';
import * as ActionCreators from './actions';

class RootNavigator extends Component {
  render() {
    return (
      <MainNavigator 
        navigation={addNavigationHelpers({
            dispatch: this.props.dispatch,
            state: this.props.nav,
        })} 
      />
    );
  }
}
  
const mapStateToProps = (state) => ({
  nav: state.nav,
  settings: state.settings,
});

const mapDispatchToProps = (dispatch) => ({ ...bindActionCreators(ActionCreators, dispatch), dispatch });
export default connect(mapStateToProps, mapDispatchToProps)(RootNavigator);
