import { createStore, applyMiddleware } from 'redux';
import thunk from 'redux-thunk';
import { composeWithDevTools } from 'redux-devtools-extension';
import reducers from '../reducers';

export default function configureStore() {
    let middleware = applyMiddleware(thunk);

    if (__DEV__) {
        middleware = composeWithDevTools(middleware);
    }

    const store = createStore(
        reducers,
        middleware
    );

    if (module.hot) {
        module.hot.accept(() => {
            const nextRootReducer = require('../reducers/').default;
            
            store.replaceReducer(nextRootReducer);
        });
    }

    return store;
}
