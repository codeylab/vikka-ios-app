import { Constants } from 'expo';

export const STATUS_BAR_HEIGHT = Constants.statusBarHeight;

// Redux Action Types
export const SET_SCROLL = 'SET_SCROLL';
export const SET_CURRENT_SCROLL = 'SET_CURRENT_SCROLL';
export const SET_LOCATION = 'SET_LOCATION';
export const SHOW_LOCATION_MODAL = 'SHOW_LOCATION_MODAL';
export const SHOW_CATEGORY_MODAL = 'SHOW_CATEGORY_MODAL';
export const SET_CATEGORY = 'SET_CATEGORY';
export const SHOW_STORE_MODAL = 'SHOW_STORE_MODAL';
export const SET_STORE = 'SET_STORE';
export const SHOW_SORT_MODAL = 'SHOW_SORT_MODAL';
export const SET_SORT = 'SET_SORT';
