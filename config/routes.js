import { StackNavigator, TabNavigator, TabBarTop } from 'react-navigation';

import HomeScreen from '../screens/HomeScreen';
import LoginScreen from '../screens/LoginScreen';
import MainScreen from '../screens/MainScreen';
import ProfileScren from '../screens/ProfileScreen';
import ForgotPassScreen from '../screens/ForgotPassScreen';
import RegisterScreen from '../screens/RegisterScreen';
import ItemScreen from '../screens/ItemScreen';
import StoreScreen from '../screens/StoreScreen';
import StoreTabScreen from '../screens/StoreTabScreen';
import SearchScreen from '../screens/SearchScreen';
import CreateStoreScreen from '../screens/CreateStoreScreen';
import CreateItemScreen from '../screens/CreateItemScreen';
import MyStoreScreen from '../screens/MyStoreScreen';
import MyAdsScreen from '../screens/MyAdsScreen';
import ApprovalAdsScreen from '../screens/ApprovalAdsScreen';
import PendingAdsScreen from '../screens/PendingAdsScreen';
import EditStroeScreen from '../screens/EditStoreScreen';

// import DemoScreen from '../screens/demoScreen';

export const MainNavigator = StackNavigator({
    Main: {
        screen: MainScreen,
        navigationOptions: () => ({
            statusBarStyle: 'dark-content',
            header: null
        }),
    },

    Login: {
        screen: LoginScreen,
        navigationOptions: () => ({
            title: 'login',
            header: null
        })
    },

    PassForgot: {
        screen: ForgotPassScreen,
        navigationOptions: () => ({
            title: 'login',            
            header: null
        })
    },

    Register: {
        screen: RegisterScreen,
        navigationOptions: () => ({
            title: 'login',            
            header: null
        })
    },

    Item: {
        screen: ItemScreen,
        navigationOptions: () => ({
            title: 'Item',            
            header: null
        })
    },

    Store: {
        screen: StoreScreen,
        navigationOptions: () => ({
            title: 'Store',            
            header: null
        })
    },

    Search: {
        screen: SearchScreen
    },
    
    CreateStore: {
        screen: CreateStoreScreen,
        navigationOptions: () => ({
            title: 'Create Store',            
            header: null
        })
    },

    CreateItem: {
        screen: CreateItemScreen,
        navigationOptions: () => ({
            title: 'Create Item',            
            header: null
        })
    },

    MyStore: {
        screen: MyStoreScreen,
        navigationOptions: () => ({
            title: 'My Store',            
            header: null
        })
    },

    MyAds: {
        screen: MyAdsScreen,
        navigationOptions: () => ({
            title: 'My Ads',            
            header: null
        })
    },

    EditStore: {
        screen: EditStroeScreen,
        navigationOptions: () => ({
            title: 'Edit Store',            
            header: null
        })
    },

    Demo: {
        screen: ProfileScren,
        navigationOptions: () => ({
            title: 'Demo Item',            
            header: null
        })
    }

});

export const MainTabNavigator = TabNavigator({
    Home: {
        screen: HomeScreen,
        navigationOptions: () => ({
            tabBarLabel: 'Browse',
        })
    },
    StoreTab: {
        screen: StoreTabScreen,
        navigationOptions: () => ({
            tabBarLabel: 'Store',
        })
    },
    Profile: {
        screen: ProfileScren,
    }
}, {
    initialRouteName: 'Home',
    tabBarPosition: 'top',
    swipeEnabled: true,
    tabBarComponent: TabBarTop,
    tabBarOptions: {
        allowFontScaling: false,
        style: {
            backgroundColor: 'red',
            padding: 8,
        },
        labelStyle: {
            color: 'white',
            fontSize: 12,
        },
        indicatorStyle: {
            borderBottomColor: '#ffffff',
            borderBottomWidth: 3,
        },
    }
});

export const AdsTabNavigator = TabNavigator({
    Approve: {
        screen: ApprovalAdsScreen,
        navigationOptions: () => ({
            tabBarLabel: 'APPROVAL ADS',
        })
    },

    Pending: {
        screen: PendingAdsScreen,
        navigationOptions: () => ({
            tabBarLabel: 'PENDING ADS',
        })
    },

}, {
    initialRouteName: 'Approve',
    tabBarPosition: 'top',
    swipeEnabled: true,
    tabBarComponent: TabBarTop,
    tabBarOptions: {
        allowFontScaling: false,
        style: {
            backgroundColor: 'red',
            padding: 8,
        },
        labelStyle: {
            color: 'white',
            fontSize: 12,
        },
        indicatorStyle: {
            borderBottomColor: '#ffffff',
            borderBottomWidth: 3,
        },
    }
});
