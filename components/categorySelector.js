import React, { Component } from 'react';
import {
    View,
    Modal,
    Text,
    FlatList,
    StatusBar
} from 'react-native';
import { NavigationActions } from 'react-navigation';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import axios from 'axios';

import * as ActionCreators from '../actions';

import ListItemOfCategory from '../components/listCategory';
import Spinner from '../components/spinner';

class CategorySelector extends Component {
    constructor(props) {
        super(props);

        this.state = {
            modalVisible: false,
            isLoading: true,
            data: null
        };
    }

    componentDidMount() {
        // if (this.props.showModal) {
        //     this.setState({
        //         modalVisible: true
        //     });
        // } else {
        //     this.setState({
        //         modalVisible: false
        //     });
        // }

        if (!this.state.data) {
            this._getData()
                .then((data) => {
                    this.setState({ data, isLoading: false });
                })
                .catch(err => { throw err; });
        }
    }

    setModalVisible(visible) {
        this.setState({
            modalVisible: visible
        });
    }

    async _getData() {
        const result = [];
        const mainCats = await axios('http://vikka.mv/core/cat_main_android.php');
        const subCats = await axios('http://vikka.mv/core/cat_sub_android.php');

        mainCats.data.maincats.map(mCat => {
            const r = { ...mCat, subData: [] };
            subCats.data.subcats.map(sCat => {
                if (sCat.main_id === mCat.group_id) {
                    r.subData.push(sCat);
                }
            });
            result.push(r);
        });

        return result;
    }

    _navigateToStore() {
        this.props.dispatch(NavigationActions.navigate({ routeName: 'Item', params: { data: this.props.item } }));
    }

    _setScroll(status) {
        this.props.setScroll(status);            
    }

    renderContent() {
        if (!this.state.isLoading) {
            return (
                <FlatList
                    onTouchStart={this._setScroll.bind(this, false)}
                    onMomentumScrollEnd={this._setScroll.bind(this, true)}
                    onScrollEndDrag={this._setScroll.bind(this, true)}
                    showsVerticalScrollIndicator={false}
                    style={styles.modalContainer}
                    data={this.state.data}
                    keyExtractor={({ group_id }) => group_id }
                    renderItem={({ item, index }) => (<ListItemOfCategory item={item} key={index} />)}
                    numColumns={1}
                />
            );
        }
        return (
            <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                <Spinner
                    size="large"
                    color="red"
                />
            </View>
        );
    }


    render() {
        return (
            <View style={{ flex: 1 }}>
                <StatusBar hidden />
                <Modal
                    animationType="slide"
                    transparent={false}
                    visible={this.props.showModal}
                    onRequestClose={() => { this.setModalVisible(false); }}
                >
                    <Text style={styles.modalTitle}>Select Category</Text>
                    {this.renderContent()}
                </Modal>
            </View>
        );
    }
}

const styles = {
     modalTitle: {
        backgroundColor: '#f50025',
        color: '#fff',
        padding: 5,
    },
    modalContainer: {
        flex: 1,
        padding: 10
    }
};

const mapStateToProps = (state) => ({
    nav: state.nav,
    tab: state.tab,
    settings: state.settings
});

const mapDispatchToProps = (dispatch) => ({ ...bindActionCreators(ActionCreators, dispatch), dispatch });

export default connect(mapStateToProps, mapDispatchToProps)(CategorySelector);
