import React, { Component } from 'react';
import {
    TouchableOpacity,
    View,
    Image,
    Text
} from 'react-native';

export default class ListItem extends Component {
    render() {
        const {
            imageUrl,
            title,
            description,
            location,
            category,
            type
        } = this.props.item;

        return (
            <View style={styles.itemContainer} key={this.props.index}>
                <View style={styles.itemImageContainer}>
                    <Image
                        style={[styles.itemImage, { height: '85%' }]}
                        source={{ uri: imageUrl }}
                    />
                </View>
                <View style={styles.itemDetailContainer}>
                    <Text style={styles.itemTitle}>{title}</Text>
                    <Text style={styles.itemDescription}>{description}</Text>
                    <View style={{ flexDirection: 'row', marginTop: 10, marginBottom: 10 }}>
                        <Text style={styles.itemLocation}>{location}, </Text>
                        <Text style={styles.itemCategory}>{category}</Text>
                    </View>
                    <Text style={styles.itemPrice}>{type}</Text>
                </View>
            </View>
        );
    }
}

const styles = {
    itemContainer: {
        flex: 1,
        // width: '100%',
        alignSelf: 'stretch',        
        flexDirection: 'row',
        borderColor: '#ddd',
        borderTopWidth: 0.5,
        borderBottomWidth: 0.5,
        paddingTop: 10,
        paddingBottom: 10
    },
    itemImageContainer: {
        borderRadius: 2,
        borderWidth: 0.5,
        borderColor: '#000',
        width: '20%',
        height: '85%',
        marginLeft: '1%'
    },
    itemDetailContainer: {
        flex: 1,
        paddingLeft: 15  
    },
    itemDescription: {
        color: '#808080',
        fontSize: 10
    },
    itemLocation: {
        color: '#808080',
        fontSize: 12
    },
    itemCategory: {
        color: '#808080',
        fontSize: 12
    },
    itemImage: {
        flex: 1,
        borderWidth: 2,
        borderRadius: 2,
        borderColor: '#fff',
        shadowColor: 'red',
        shadowOpacity: 0.5,
        shadowOffset: { height: 2, width: 2 },
    }
};
