import React, { Component } from 'react';
import {
    TouchableOpacity,
    Text,
    AsyncStorage,
} from 'react-native';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { NavigationActions } from 'react-navigation';
import Panel from '../components/Panel';

import * as ActionCreators from '../actions';

class ListItemOfCategory extends Component {
    _navigateToStore() {
        this.props.dispatch(NavigationActions.navigate({ routeName: 'Item', params: { data: this.props.item } }));
    }

    async _setCategory(item) {
        await AsyncStorage.setItem('category', JSON.stringify(item));
        const newItem = await AsyncStorage.getItem('category');
        this.props.setCategory(JSON.parse(newItem));
        this.props.showCategoryModal(false);
    }

    render() {
        const _renderSubCategoryList = this.props.item.subData.map((d, i) => {
            return (
                <TouchableOpacity key={i} style={styles.categoryItemContainer} onPress={this._setCategory.bind(this, d)}>
                    <Text>{d.child_name}</Text>
                </TouchableOpacity>
            );
         });

        return (
            <Panel key={this.props.item.group_id} title={this.props.item.group_name}>
                {_renderSubCategoryList}
            </Panel>
         );
    }
}

const styles = {
    modalItem: {
        padding: 5,
        borderBottomWidth: 0.5,
        borderColor: '#c7c7cd'
    },
    categoryItemContainer: {
        borderBottomWidth: 0.5,
        borderColor: '#ddd',
        paddingTop: 5,
        paddingBottom: 5
    }
};

const mapStateToProps = (state) => ({
    nav: state.nav,
    tab: state.tab,
    settings: state.settings,
    store: state.store
});

const mapDispatchToProps = (dispatch) => ({ ...bindActionCreators(ActionCreators, dispatch), dispatch });

export default connect(mapStateToProps, mapDispatchToProps)(ListItemOfCategory);
