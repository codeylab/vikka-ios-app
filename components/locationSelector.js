import React, { Component } from 'react';
import {
    View,
    Modal,
    Text,
    FlatList,
    StatusBar
} from 'react-native';
import { NavigationActions } from 'react-navigation';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import axios from 'axios';

import * as ActionCreators from '../actions';

import ListItemOfLocation from '../components/listLocation';
import Spinner from '../components/spinner';

class LocationSelector extends Component {
    constructor(props) {
        super(props);

        this.state = {
            modalVisible: false,
            isLoading: true,
            data: null
        };
    }

    componentDidMount() {
        if (!this.state.data) {
            this._getData()
                .then((data) => {
                    this.setState({ data: data.locations, isLoading: false });
                })
                .catch(err => { throw err; });
        }
    }

    setModalVisible(visible) {
        this.setState({
            modalVisible: visible
        });
    }

    async _getData() {
        const res = await axios('http://vikka.mv/core/ls_locations.php');
        return res.data;
    }

    _navigateToStore() {
        this.props.dispatch(NavigationActions.navigate({ routeName: 'Item', params: { data: this.props.item } }));
    }

    _setScroll(status) {
        this.props.setScroll(status);            
    }

    renderContent() {
        if (!this.state.isLoading) {
            return (
                <FlatList
                    onTouchStart={this._setScroll.bind(this, false)}
                    onMomentumScrollEnd={this._setScroll.bind(this, true)}
                    onScrollEndDrag={this._setScroll.bind(this, true)}
                    showsVerticalScrollIndicator={false}
                    style={styles.modalContainer}
                    data={this.state.data}
                    keyExtractor={({ city_id }) => city_id}
                    renderItem={({ item, index }) => (<ListItemOfLocation item={item} key={index} />)}
                    numColumns={1}
                />
            );
        }
        return (
            <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                <Spinner
                    size="large"
                    color="red"
                />
            </View>
        );
    }


    render() {
        return (
            <View style={{ flex: 1 }}>
                 <StatusBar hidden />
                <Modal
                    style={{ flex: 1 }}
                    animationType="slide"
                    transparent={false}
                    visible={this.props.showModal}
                    onRequestClose={() => { this.setModalVisible(false); }}
                >
                    <Text style={styles.modalTitle}>Select Location</Text>
                    {this.renderContent()}
                </Modal>
            </View>
        );
    }
}

const styles = {
     modalTitle: {
        backgroundColor: '#f50025',
        color: '#fff',
        padding: 5,
    },
    modalContainer: {
        flex: 1,
        padding: 10
    }
};

const mapStateToProps = (state) => ({
    nav: state.nav,
    tab: state.tab,
    settings: state.settings
});

const mapDispatchToProps = (dispatch) => ({ ...bindActionCreators(ActionCreators, dispatch), dispatch });

export default connect(mapStateToProps, mapDispatchToProps)(LocationSelector);
