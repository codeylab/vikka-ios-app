import React, { Component } from 'react';
import {
	StyleSheet,
	View,
	TextInput,
	Dimensions,
} from 'react-native';

export default class LoginInputText extends Component {
	render() {
		return (
			<View>
                <TextInput 
                    style={[
                        styles.input, 
                        (this.props.topRound) ? styles.topRound : null,
                        (this.props.bottomRound) ? styles.bottomRound : null,
                    ]}
                    placeholder={this.props.placeholder}
                    secureTextEntry={this.props.secureTextEntry}
                    autoCorrect={this.props.autoCorrect}
                    returnKeyType={this.props.returnKeyType}
                    placeholderTextColor='#d3d3d3'
                    underlineColorAndroid='transparent' 
                    value={this.props.value}
                    onChangeText={this.props.onChangeText}
                />
			</View>
		);
	}
}

const DEVICE_WIDTH = Dimensions.get('window').width;

const styles = StyleSheet.create({
	input: {
		backgroundColor: '#eee',
		width: DEVICE_WIDTH - 40,
		height: 48,
		marginHorizontal: 20,
		paddingLeft: 5,
        fontSize: 18,
	},
    topRound: {
        borderTopLeftRadius: 5,
        borderTopRightRadius: 5
    },
    bottomRound: {
        borderBottomLeftRadius: 5,
        borderBottomRightRadius: 5
    }
});
