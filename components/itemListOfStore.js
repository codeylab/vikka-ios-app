import React, { Component } from 'react';
import {
    TouchableHighlight,
    View,
    Image,
    Text
} from 'react-native';
import { NavigationActions } from 'react-navigation';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import * as ActionCreators from '../actions';

class ItemListOfStore extends Component {
    _navigateToStore() {
        this.props.dispatch(NavigationActions.navigate({ routeName: 'Item', params: { data: this.props.item } }));
    }

    render() {
        const {
            ls_ad_id,
            ct1_id,
            ct2_id,
            ct1_name,
            ct2_name,
            ls_location_id,
            ls_price,
            ls_contact,
            ls_heading,
            ls_description,
            ls_post_date,
            ls_store_id,
            ls_ad_status,
            ls_store_name,
            ls_favorite,
            ls_loc_address,
            ls_image,
        } = this.props.item;

        const {
                itemContainer,
                itemImageContainer,
                itemDetailContainer,
                itemTitle,
                itemDescription,
                itemLocation,
                itemCategory,
                itemPrice,
                itemImage
            } = styles;

        return (
            <TouchableHighlight key={this.props.index} underlayColor="#eee" onPress={() => this._navigateToStore()}>
                <View style={itemContainer}>
                    <View style={itemImageContainer}>
                        <Image
                            style={[itemImage]}
                            source={{ uri: `http://vikka.mv/images/adds/adds/${ls_image}` }}
                        />
                    </View>
                    <View style={itemDetailContainer}>
                        <Text style={itemTitle}>{ls_heading}</Text>
                        <Text style={itemDescription}>{`${(ls_description).replace(/(\r\n|\n|\r)/gm, '').substring(0, 50)}...`}</Text>
                        <View style={{ flexDirection: 'row', marginTop: 10 }}>
                            <Text style={itemLocation}>{ls_loc_address}, </Text>
                            <Text style={itemCategory}>{ct1_name}</Text>
                        </View>
                        <Text style={itemPrice}>MVR {ls_price}</Text>
                    </View>
                </View>
            </TouchableHighlight>
        );
    }
}

const styles = {
    itemContainer: {
        flex: 1,
        flexDirection: 'row',
        borderColor: '#ddd',
        backgroundColor: '#fff',
        borderTopWidth: 0.5,
        borderBottomWidth: 0.5,
        paddingTop: 10,
        paddingBottom: 10
    },
    itemImageContainer: {
        flex: 1,
        borderRadius: 2,
        borderWidth: 0.5,
        borderColor: '#000',
        height: 82,
    },
    itemDetailContainer: {
        flex: 3,
        paddingLeft: 15  
    },
    itemTitle: {

    },
    itemDescription: {
        color: '#808080',
        fontSize: 10
    },
    itemLocation: {
        color: '#808080',
        fontSize: 10        
    },
    itemCategory: {
        color: '#808080',
        fontSize: 10        
    },
    itemImage: {
        flex: 1,
        borderWidth: 2,
        borderRadius: 2,
        borderColor: '#fff',
        shadowColor: 'red',
        shadowOpacity: 0.5,
        height: 80,
        resizeMode: 'cover',
        shadowOffset: { height: 2, width: 2 },
    }
};

const mapStateToProps = (state) => ({
    nav: state.nav,
    tab: state.tab,
    settings: state.settings
});

const mapDispatchToProps = (dispatch) => ({ ...bindActionCreators(ActionCreators, dispatch), dispatch });

export default connect(mapStateToProps, mapDispatchToProps)(ItemListOfStore);
