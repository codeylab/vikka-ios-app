import React, { Component } from 'react';
import {
    TouchableOpacity,
    Text,
    View,
    Image
} from 'react-native';
import { NavigationActions } from 'react-navigation';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import * as ActionCreators from '../actions';

class storeListItem extends Component {
    _navigateToStore() {
        this.props.dispatch(NavigationActions.navigate({ routeName: 'Item', params: { data: this.props.item } }));
    }

    async _setStore(item) {
        this.props.setStore(item);
        this.props.showStoreModal(false);
    }

    render() {
        const {
            st_name,
            st_logo
        } = this.props.item;

        const url = `http://www.vikka.mv/images/store/${st_logo}`;

        return (
            <View style={{ flex: 1 }}>
                <TouchableOpacity style={styles.modalItem} key={this.props.index} onPress={this._setStore.bind(this, this.props.item)}>
                    <View style={{ flex: 1, flexDirection: 'row', justifyContent: 'flex-start' }}>
                        <Image 
                            style={{ width: 40, height: 40, }}
                            source={{ uri: url }} 
                        />
                        <View style={{ flex: 1, alignSelf: 'center' }}>
                            <Text style={{ alignSelf: 'center', justifyContent: 'flex-end', fontSize: 16, textAlign: 'right' }}>{st_name}</Text>
                        </View>
                    </View>
                </TouchableOpacity>
            </View>
        );
    }
}

const styles = {
    modalItem: {
        padding: 5,
        borderBottomWidth: 0.5,
        borderColor: '#c7c7cd'
    }
};

const mapStateToProps = (state) => ({
    nav: state.nav,
    tab: state.tab,
    settings: state.settings
});

const mapDispatchToProps = (dispatch) => ({ ...bindActionCreators(ActionCreators, dispatch), dispatch });

export default connect(mapStateToProps, mapDispatchToProps)(storeListItem);
