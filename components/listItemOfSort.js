import React, { Component } from 'react';
import {
    TouchableOpacity,
    Text,
    View,
    Image
} from 'react-native';
import { NavigationActions } from 'react-navigation';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { CheckBox } from 'react-native-elements';

import * as ActionCreators from '../actions';

class ListItemOfSort extends Component {
    _navigateToStore() {
        this.props.dispatch(NavigationActions.navigate({ routeName: 'Item', params: { data: this.props.item } }));
    }

    async _setSort(item) {
        this.props.setSort(item);
        this.props.showSortModal(false);
    }

    render() {
        const {
            name,
            value
        } = this.props.item;

        const checked = (this.props.store.sort.value === value);

        return (
            <View style={{ flex: 1 }}>
                <View style={{ flex: 1, justifyContent: 'center', alignSelf: 'flex-start', width: '95%' }}>
                    <CheckBox
                        containerStyle={{ width: '100%' }}
                        onPress={() => this._setSort(this.props.item)}
                        title={name}
                        checked={checked}
                    />
                </View>
            </View>
        );
    }
}

const mapStateToProps = (state) => ({
    nav: state.nav,
    tab: state.tab,
    settings: state.settings,
    store: state.store
});

const mapDispatchToProps = (dispatch) => ({ ...bindActionCreators(ActionCreators, dispatch), dispatch });

export default connect(mapStateToProps, mapDispatchToProps)(ListItemOfSort);
