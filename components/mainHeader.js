import React, { Component } from 'react';
import { View, Text, Platform } from 'react-native';
import { Header } from 'react-native-elements';
import Icon from '@expo/vector-icons/Feather';

import SearchBarContainer from './searchBarContainer';

class SearchIconContainer extends Component {
    render() {
        return (
            <View style={styles.searchIconContainer}>
                <Icon name="message-circle" size={24} color="#FFF" />
                <Text style={styles.textStyle}>Chat</Text>
            </View>
        );
    }
}

export default class MainHeader extends Component {
  state = {
      text: ''
  }

  render() {    
    return (
        <Header
            style={styles.headerContainer}
            leftComponent={<SearchBarContainer />}
        />  
    );
  }
}

const styles = {
    headerContainer: {
        height: 56, 
        backgroundColor: 'red', 
    },
    containerStyle: {
        backgroundColor: 'red', 
        width: Platform.OS === 'android' ? 300 : 260, 
        marginLeft: 3,
        paddingTop: 0,
        borderBottomWidth: 0,
        borderTopWidth: 0,
        marginBottom: Platform.OS === 'android' ? 3 : 0
    },
    inputStyle: {
        height: 40, 
        backgroundColor: '#FFF', 
        color: '#000', 
        fontSize: 16, 
    },
    textStyle: {
        color: '#FFF'
    },
    searchIconContainer: {
        flex: 1, 
        alignSelf: 'flex-end',
        marginRight: 12, 
        marginTop: 5
    }
};
