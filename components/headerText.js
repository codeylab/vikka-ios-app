import React from 'react';
import { 
    Text,
} from 'react-native';

const HeaderText = ({ title, marginLeft = 20 }) => (
    <Text style={[styles.titleText, { marginLeft }]} >
        {title}
    </Text>
);

const styles = {
    titleText: {
        color: '#fff',
        fontSize: 18,
        fontWeight: '500',
    }
};

export default HeaderText;
