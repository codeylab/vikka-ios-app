import React, { Component } from 'react';
import { Platform, Dimensions } from 'react-native';
import { SearchBar } from 'react-native-elements';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { NavigationActions } from 'react-navigation';

import * as ActionCreators from '../actions';

class SearchBarContainer extends Component {
    constructor(props) {
        super(props);

        this.state = {
            searchText: ''
        };
    }

    _search() {
        this.props.dispatch(
            NavigationActions.navigate({
              routeName: 'Search',
              params: { text: this.state.searchText },
            }),
          );
    }

    render() {
        return (
            <SearchBar
                containerStyle={styles.containerStyle}
                inputStyle={styles.inputStyle}
                lightTheme 
                clearIcon
                onSubmitEditing={this._search.bind(this)}
                onChangeText={(text) => this.setState({ searchText: text })}
            />
        );
    }
}

const styles = {
    containerStyle: {
        backgroundColor: 'red', 
        width: Dimensions.get('window').width - 5,
        marginLeft: 3,
        paddingTop: 0,
        borderBottomWidth: 0,
        borderTopWidth: 0,
        marginBottom: Platform.OS === 'android' ? 3 : 0
    },
    inputStyle: {
        height: 40, 
        backgroundColor: '#FFF', 
        color: '#000', 
        fontSize: 16, 
    },
};

const mapStateToProps = (state) => ({
    tab: state.tab,
    nav: state.nav,
    settings: state.settings,
});

const mapDispatchToProps = (dispatch) => ({ ...bindActionCreators(ActionCreators, dispatch), dispatch });

export default connect(mapStateToProps, mapDispatchToProps)(SearchBarContainer);
