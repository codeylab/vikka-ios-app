import React, { Component } from 'react';
import {
    TouchableOpacity,
    Text,
    AsyncStorage
} from 'react-native';
import { NavigationActions } from 'react-navigation';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import * as ActionCreators from '../actions';

class ListItemOfLocation extends Component {
    _navigateToStore() {
        this.props.dispatch(NavigationActions.navigate({ routeName: 'Item', params: { data: this.props.item } }));
    }

    async _setLocation(item) {
        await AsyncStorage.setItem('location', JSON.stringify(item));
        const newItem = await AsyncStorage.getItem('location');
        this.props.setLocation(JSON.parse(newItem));
        this.props.showLocationModal(false);
    }

    render() {
        const {
            city_name
        } = this.props.item;

        return (
            <TouchableOpacity style={styles.modalItem} key={this.props.index} onPress={this._setLocation.bind(this, this.props.item)}>
                    <Text>{city_name}</Text>
            </TouchableOpacity>
        );
    }
}

const styles = {
    modalItem: {
        padding: 5,
        borderBottomWidth: 0.5,
        borderColor: '#c7c7cd'
    }
};

const mapStateToProps = (state) => ({
    nav: state.nav,
    tab: state.tab,
    settings: state.settings
});

const mapDispatchToProps = (dispatch) => ({ ...bindActionCreators(ActionCreators, dispatch), dispatch });

export default connect(mapStateToProps, mapDispatchToProps)(ListItemOfLocation);
