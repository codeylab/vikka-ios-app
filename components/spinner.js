import React, { Component } from 'react';
import {
	StyleSheet,
	View,
    ActivityIndicator
} from 'react-native';

export default class Spinner extends Component {
	render() {
		return (
			<View>
                <ActivityIndicator
                    size={this.props.size}
                    color={this.props.color}
                />
			</View>
		);
	}
}

const styles = StyleSheet.create({

});
