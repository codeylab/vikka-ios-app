import React, { Component } from 'react';
import {
	StyleSheet,
	View,
    Image,
    Text
} from 'react-native';

export default class DetailCard extends Component {
	render() {
        const { cardContainer,
            cardTitleContainer,
            cardTitle,
            cardRowContainer,
            cardBodyContainer,
            cardImageContainer,
            cardTextContainer
        } = styles;

        const renderContent = () => {
            const data = this.props.data;
            const type = this.props.type;

            if (type === 'singleLine') {
                return (
                    <View style={cardBodyContainer}>
                        <Text style={{ textAlign: 'justify' }}>{data}</Text>
                    </View>
                );
            } else if (type === 'multiLine') {
                const dataList = data.map((row, index) => {
                    return (
                        <View key={index} style={cardRowContainer}>
                            <Text style={{ flex: 1, color: '#757575' }}>{Object.keys(row)}</Text>
                            <Text style={{ flex: 3 }}>{row[Object.keys(row)]}</Text>
                        </View>
                    );
                });

                return (
                    <View style={cardBodyContainer}>
                        {dataList}                    
                    </View>
                );
            } else if (type === 'staticLine') {
                return (
                    <View style={cardBodyContainer}>
                        <View style={cardImageContainer}>
                                <Image
                                    style={{ flex: 1, resizeMode: 'contain' }}
                                    source={{ uri: 'https://www.kelp4less.com/wp-content/uploads/2014/12/Buyer-Protection-Badge-small-150.png' }}
                                />
                            <View style={{ flex: 3 }}>
                                <Text style={{ color: '#757575' }}>Meet the seller in person, check the item and make sure you are satisfied with it before you make a payment</Text>
                            </View>   
                        </View>
                            <View style={cardTextContainer}>
                                <Text>Exchange item and payment at the same time</Text>
                                <Text style={{ color: '#757575', marginTop: 10 }}>Never give out financial information</Text>
                            </View>                       
                    </View>
                );
            }
        };

		return (
			<View style={cardContainer} elevation={1}>
                <View style={cardTitleContainer}>
                    <Text style={cardTitle}>{this.props.title}</Text>
                </View>
                {renderContent()}
            </View>
		);
	}
}

const styles = StyleSheet.create({
    cardContainer: {
        flex: 1,
        marginTop: 10,
        backgroundColor: '#fff',        
    },
    cardTitleContainer: {
        padding: 15,
        borderBottomColor: '#e4e4e4',
        borderBottomWidth: 0.5
    },
    cardTitle: {
        fontWeight: 'bold'
    },
    cardBodyContainer: {
        flex: 1,
        padding: 10,
        paddingLeft: 15
    },
    cardImageContainer: {
        flexDirection: 'row'
    },
    cardRowContainer: {
        flexDirection: 'row',
        padding: 5,
        paddingLeft: 0
    },
    cardTextContainer: {
        marginTop: 20
    }
});
