import React, { Component } from 'react';
import {
    View,
    Modal,
    Text,
    FlatList,
    StatusBar
} from 'react-native';
import { NavigationActions } from 'react-navigation';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import * as ActionCreators from '../actions';

import ListItemOfSort from '../components/listItemOfSort';
import Spinner from '../components/spinner';

class SortSelector extends Component {
    constructor(props) {
        super(props);

        this.state = {
            modalVisible: false,
            isLoading: false,
            data: [
                { name: 'All', value: 1 },
                { name: 'Low Price', value: 2 },
                { name: 'High Price', value: 3 },
                { name: 'Recent Post', value: 4 },
                { name: 'Old Post', value: 5 },
            ]
        };
    }

    setModalVisible(visible) {
        this.setState({
            modalVisible: visible
        });
    }

    _navigateToStore() {
        this.props.dispatch(NavigationActions.navigate({ routeName: 'Item', params: { data: this.props.item } }));
    }

    _setScroll(status) {
        this.props.setScroll(status);            
    }

    renderContent() {
        if (!this.state.isLoading) {
            return (
                <FlatList
                    onTouchStart={this._setScroll.bind(this, false)}
                    onMomentumScrollEnd={this._setScroll.bind(this, true)}
                    onScrollEndDrag={this._setScroll.bind(this, true)}
                    showsVerticalScrollIndicator={false}
                    style={styles.modalContainer}
                    data={this.state.data}
                    keyExtractor={({ value }) => value}
                    renderItem={({ item, index }) => (<ListItemOfSort item={item} key={index} />)}
                    numColumns={1}
                />
            );
        }
        return (
            <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                <Spinner
                    size="large"
                    color="red"
                />
            </View>
        );
    }


    render() {
        return (
            <View style={{ flex: 1 }}>
                <StatusBar hidden />
                <Modal
                    animationType="slide"
                    transparent={false}
                    visible={this.props.showModal}
                    onRequestClose={() => { this.setModalVisible(false); }}
                >
                    <Text style={styles.modalTitle}>Sort Ads</Text>
                    {this.renderContent()}
                </Modal>
            </View>
        );
    }
}

const styles = {
     modalTitle: {
        backgroundColor: '#f50025',
        color: '#fff',
        padding: 5,
    },
    modalContainer: {
        flex: 1,
        padding: 10
    }
};

const mapStateToProps = (state) => ({
    nav: state.nav,
    tab: state.tab,
    settings: state.settings
});

const mapDispatchToProps = (dispatch) => ({ ...bindActionCreators(ActionCreators, dispatch), dispatch });

export default connect(mapStateToProps, mapDispatchToProps)(SortSelector);
