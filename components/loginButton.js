import React, { Component } from 'react';
import {
	StyleSheet,
	View,
    TouchableOpacity,
    Text
} from 'react-native';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import * as ActionCreators from '../actions';

class LoginButton extends Component {

	render() {
        const { buttonContainer, buttonText, button } = styles;
		return (
			<View style={buttonContainer}>
                <TouchableOpacity style={button} onPress={() => this.props.onPressBtn()}>
                    <Text style={buttonText}>{this.props.buttonText}</Text>
                </TouchableOpacity>
            </View>
		);
	}
}

const styles = StyleSheet.create({
    buttonContainer: {
        marginTop: 20,
        paddingLeft: 20,
        paddingRight: 20,
    },
    button: {
        height: 48,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#380813',
        borderRadius: 5
    },
    buttonText: {
        color: '#fff',
        fontWeight: 'bold',
    }
});

const mapStateToProps = (state) => ({
    nav: state.nav,
    settings: state.settings
});

const mapDispatchToProps = (dispatch) => ({ ...bindActionCreators(ActionCreators, dispatch), dispatch });

export default connect(mapStateToProps, mapDispatchToProps)(LoginButton);
