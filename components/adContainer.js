import React, { Component } from 'react';
import { View, Image, Dimensions } from 'react-native';

const width = Dimensions.get('window').width;

class AdContainer extends Component {
    render() {
        return (
            <View style={styles.container}>
                <Image 
                    style={styles.image}
                    resizeMode="stretch"
                    source={{ uri: this.props.image }}
                />
            </View>
        );
    }
}

const styles = {
    container: {
        height: 180,
    },
    image: {
        height: 180,
        width
    }
};

export default AdContainer;
