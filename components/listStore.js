import React, { Component } from 'react';
import {
    TouchableOpacity,
    View,
    Image,
    Text
} from 'react-native';
import { NavigationActions } from 'react-navigation';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import * as ActionCreators from '../actions';

class ListStore extends Component {
    _navigateToStore(id) {
        this.props.dispatch(NavigationActions.navigate({ routeName: 'Store', params: { st_id: id } }));
    }

    render() {
        const {
            st_id,
            st_logo,
            st_name,
            st_desc,
            st_loc,
            st_type
        } = this.props.item;

        return (
            <TouchableOpacity key={this.props.index} onPress={() => this._navigateToStore(st_id)}>
                <View style={styles.itemContainer}>
                    <View style={styles.itemImageContainer}>
                        <Image
                            style={[styles.itemImage, { height: '85%' }]}
                            source={{ uri: `http://vikka.mv/images/store/${st_logo}` }}
                        />
                    </View>
                    <View style={styles.itemDetailContainer}>
                        <Text style={styles.itemTitle}>{st_name}</Text>
                        <Text style={styles.itemDescription}>{st_desc}</Text>
                        <View style={{ flexDirection: 'row', marginTop: 10 }}>
                            <Text style={styles.itemLocation}>{st_loc}</Text>
                        </View>
                        <Text style={styles.itemPrice}>{st_type}</Text>
                    </View>
                </View>
            </TouchableOpacity>
        );
    }
}

const styles = {
    itemContainer: {
        flex: 1,
        // width: '100%',
        alignSelf: 'stretch',        
        flexDirection: 'row',
        borderColor: '#ddd',
        backgroundColor: '#fff',
        borderTopWidth: 0.5,
        borderBottomWidth: 0.5,
        paddingTop: 10,
        paddingBottom: 10
    },
    itemImageContainer: {
        borderRadius: 2,
        borderWidth: 0.5,
        backgroundColor: '#ddd',
        borderColor: '#000',
        width: 65,
        height: 70,
        marginLeft: '1.5%'
    },
    itemDetailContainer: {
        flex: 1,
        paddingLeft: 15  
    },
    itemDescription: {
        color: '#808080',
        fontSize: 10
    },
    itemLocation: {
        color: '#808080',
    },
    itemCategory: {
        color: '#808080',
    },
    itemImage: {
        flex: 1,
        borderWidth: 2,
        borderRadius: 2,
        borderColor: '#fff',
        shadowColor: 'red',
        shadowOpacity: 0.5,
        shadowOffset: { height: 2, width: 2 },
    }
};

const mapStateToProps = (state) => ({
    nav: state.nav,
    tab: state.tab,
    settings: state.settings
});

const mapDispatchToProps = (dispatch) => ({ ...bindActionCreators(ActionCreators, dispatch), dispatch });

export default connect(mapStateToProps, mapDispatchToProps)(ListStore);
