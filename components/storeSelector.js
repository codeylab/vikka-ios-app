import React, { Component } from 'react';
import {
    View,
    Modal,
    Text,
    FlatList,
    AsyncStorage
} from 'react-native';
import { NavigationActions } from 'react-navigation';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import axios from 'axios';

import * as ActionCreators from '../actions';

import ListItemOfStore from '../components/storeListItem';
import Spinner from '../components/spinner';

class StoreSelector extends Component {
    constructor(props) {
        super(props);

        this.state = {
            modalVisible: false,
            isLoading: true,
            data: null
        };
    }

    componentDidMount() {
        if (!this.state.data) {
            this._getData()
                .then((data) => {
                    if (data.store == null) {
                        this.setModalVisible(false);
                    } else {
                        this.setState({ data: data.store, isLoading: false });
                    }
                    
                })
                .catch(err => { 
                    this.setModalVisible(false);
                    throw err; 
                });
        } 
    }

    setModalVisible(visible) {
        this.setState({
            modalVisible: visible
        });
    }

    async _getData() {
        let user = await AsyncStorage.getItem('user');
        user = JSON.parse(user);
        if (user !== null) {
            user = user[0];
            const url = `http://vikka.mv/core/getMyStoreList.php?email=${user.uEmail_h}`;
            const { data } = await axios.get(url);
            return data;
        }
    }

    _navigateToStore() {
        this.props.dispatch(NavigationActions.navigate({ routeName: 'Item', params: { data: this.props.item } }));
    }

    _setScroll(status) {
        this.props.setScroll(status);            
    }

    renderContent() {
        if (!this.state.isLoading) {
            return (
                <FlatList
                    onTouchStart={this._setScroll.bind(this, false)}
                    onMomentumScrollEnd={this._setScroll.bind(this, true)}
                    onScrollEndDrag={this._setScroll.bind(this, true)}
                    showsVerticalScrollIndicator={false}
                    style={styles.modalContainer}
                    data={this.state.data}
                    keyExtractor={({ st_id }) => st_id}
                    renderItem={({ item, index }) => (<ListItemOfStore item={item} key={index} />)}
                    numColumns={1}
                />
            );
        }
        return (
            <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                <Spinner
                    size="large"
                    color="red"
                />
            </View>
        );
    }


    render() {
        return (
            <View style={{ flex: 1 }}>
                <Modal
                    animationType="slide"
                    transparent={false}
                    visible={this.props.showModal && this.state.data != null}
                    onRequestClose={() => { this.setModalVisible(false); }}
                >
                    <Text style={styles.modalTitle}>Select Store</Text>
                    {this.renderContent()}
                </Modal>
            </View>
        );
    }
}

const styles = {
     modalTitle: {
        backgroundColor: '#f50025',
        color: '#fff',
        padding: 5,
    },
    modalContainer: {
        flex: 1,
        padding: 10
    }
};

const mapStateToProps = (state) => ({
    nav: state.nav,
    tab: state.tab,
    settings: state.settings
});

const mapDispatchToProps = (dispatch) => ({ ...bindActionCreators(ActionCreators, dispatch), dispatch });

export default connect(mapStateToProps, mapDispatchToProps)(StoreSelector);
