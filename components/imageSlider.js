import React, { Component } from 'react';
import {
	StyleSheet,
	View,
    Dimensions,
    Image
} from 'react-native';
import Carousel from 'react-native-carousel';

export default class ImageSlider extends Component {
	render() {
		return (
			<View style={{ height: HEIGHT / 3 }}>
                <Carousel 
                    width={WIDTH}
                    indicatorSize={40}
                    indicatorOffset={1}
                    inactiveIndicatorText='•' 
                    indicatorText='•'
                    indicatorAtBottom
                    animate={false}
                    loop={false}
                >
                    {this.props.images[0] && <View style={styles.sliderContainer}>
                        <Image
                            style={{ flex: 1, width: Dimensions.get('window').width }}
                            source={{ uri: `http://www.vikka.mv/images/adds/adds/${this.props.images[0].im_path}` }}
                        />
                    </View>}

                    {this.props.images[1] && <View style={styles.sliderContainer}>
                        <Image
                            style={{ flex: 1, width: Dimensions.get('window').width }}
                            source={{ uri: `http://www.vikka.mv/images/adds/adds/${this.props.images[1].im_path}` }}
                        />
                    </View>}

                    {this.props.images[2] && <View style={styles.sliderContainer}>
                        <Image
                            style={{ flex: 1, width: Dimensions.get('window').width }}
                            source={{ uri: `http://www.vikka.mv/images/adds/adds/${this.props.images[2].im_path}` }}
                        />
                    </View>}

                    {this.props.images[3] && <View style={styles.sliderContainer}>
                        <Image
                            style={{ flex: 1, width: Dimensions.get('window').width }}
                            source={{ uri: `http://www.vikka.mv/images/adds/adds/${this.props.images[3].im_path}` }}
                        />
                    </View>}

                </Carousel>
            </View>
		);
	}
}

const HEIGHT = Dimensions.get('window').height;
const WIDTH = Dimensions.get('window').width;

const styles = StyleSheet.create({
    sliderContainer: {
        flex: 1,
        backgroundColor: '#eee',
    }
});
