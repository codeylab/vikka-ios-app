import React, { Component } from 'react';
import { 
    ScrollView,
    View,
    Image,
    ImageBackground,
    TouchableOpacity,
    Text,
    Dimensions,
    TextInput,
    Modal,
    AsyncStorage,
    Alert
} from 'react-native';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { NavigationActions } from 'react-navigation';
import { Icon } from 'react-native-elements';
import { ImagePicker } from 'expo';
import axios from 'axios';

import * as ActionCreators from '../actions';
import Spinner from '../components/spinner';

class ProfileScreen extends Component {
    constructor(props) {
        super(props);
        this.state = {
            mobile: '',
            email: '',
            locationName: '',
            userName: '',
            isOnUpdate: false,
            imageSelectModal: false,
            image: null,
            modalLocationVisible: false,
            isLoading: true,
            logedIn: false,
            user: null
        };
    }

    async componentDidMount() {
        try {
            let isLogedIn = await AsyncStorage.getItem('logedIn');
            let user = await AsyncStorage.getItem('user');
    
            isLogedIn = JSON.parse(isLogedIn);
            user = JSON.parse(user)[0];  
            
            this.setState({
                logedIn: isLogedIn,
                user
            });
        } catch (e) {
            this.setState({
                user: null,
                logedIn: false
            });
        } 

        if (this.state.logedIn) {
            this._getData()
                .then((data) => {
                    this.setState({
                        mobile: data.user_profile[0].uMobile_h,
                        email: data.user_profile[0].uEmail_h,
                        locationName: data.user_profile[0].uAdd_h,
                        userName: data.user_profile[0].uName,
                        image: `http://www.vikka.mv/images/profile_pic/${data.user_profile[0].uImage}`,
                        isLoading: false
                    });
                })
                .catch(err => { throw err; });
        }
    }
    
    setImageSlectModalVisible(visible) {
        this.setState({ imageSelectModal: visible });
    }

    async _getData() {
        // const { state } = this.props.navigation;
        const res = await axios(`http://vikka.mv/core/user_profile.php?user_email=${this.state.user.uEmail_h}`);
        return res.data;            
    }

    _pickImage = async () => {
        const result = await ImagePicker.launchImageLibraryAsync({
            allowsEditing: false,
            aspect: [4, 3],
        }); 
    
        if (!result.cancelled) {
            this.setState({ image: result.uri });
        }

        this.setImageSlectModalVisible(false);
    };

    _takePhoto = async () => {
        const { Permissions } = Expo;
        const permissions = Permissions.CAMERA;
        const status = await Permissions.askAsync(permissions);

        if (status.status !== 'granted') {
            this.setimageSlectModalVisible(false, null);    
            Alert.alert('Permission Rejected', 'Sorry, We can\'t access the camera');
        } else {
            const result = await ImagePicker.launchCameraAsync({
                allowsEditing: false,
                aspect: [4, 3],
            });
    
            if (!result.cancelled) {
                if (!result.cancelled) {
                    this.setState({ image: result.uri });
                }
            }
    
            this.setImageSlectModalVisible(false);      
        }

    };

    _removeImage = async () => {
        this.setState({ image: null });
        
        this.setImageSlectModalVisible(false);        
    }

    doNavigate() {
        this.props.dispatch(NavigationActions.navigate({ routeName: 'Login' }));
    }
    
    _toggleUpdate(action) {
        if (action) {
            this._saveData()
                .then(() => {
                    this.setState({ isOnUpdate: !this.state.isOnUpdate });
                })
                .catch(err => { throw err; });
        }
        
        if (!action) {
            this.setState({
                isOnUpdate: !this.state.isOnUpdate
            });
        }
    }

    async _saveData() {
        const { email, userName, locationName, mobile } = this.state;
        const res = await axios(`http://vikka.mv/core/user_profile_save.php?uName=${userName}&uMobile_h=${mobile}&uAdd_h=${locationName}&uEmail_h=${email}`);
        return res.status;
    }

    renderImageSelectionModal() {
        const { imageSlectModal, imageSelectionButton } = styles;
        const WIDTH = Dimensions.get('window').width;

        return (
            <Modal
                animationType="fade"
                transparent
                visible={this.state.imageSelectModal}
                onRequestClose={() => { this.setImageSlectModalVisible(false); }}
            >
                <View 
                    style={{
                        flex: 1,
                        justifyContent: 'center',
                        alignItems: 'center',
                        backgroundColor: 'rgba(0,0,0,0.5)'
                    }}
                >
                    <View style={[imageSlectModal, { width: WIDTH - 20 }]}>
                        <View style={{ padding: 5, alignItems: 'center' }}>
                            <Text>Choose a photo</Text>
                        </View>
                        <TouchableOpacity style={imageSelectionButton} onPress={() => this._takePhoto(this.state.placeOfModal)}>
                            <Text style={{ color: '#fff' }}>FROM CAMERA</Text>
                        </TouchableOpacity>

                        <TouchableOpacity style={imageSelectionButton} onPress={() => this._pickImage(this.state.placeOfModal)}>
                            <Text style={{ color: '#fff' }}>FROM GALLERY</Text>
                        </TouchableOpacity>

                        <TouchableOpacity onPress={() => this._removeImage(this.state.placeOfModal)} style={[imageSelectionButton, { backgroundColor: '#f50025' }]}>
                            <Text style={{ color: '#fff' }}>REMOVE IMAGE</Text>
                        </TouchableOpacity>

                        <TouchableOpacity onPress={() => this.setImageSlectModalVisible(false)} style={[imageSelectionButton, { backgroundColor: '#ddd' }]}>
                            <Text style={{ color: '#fff' }}>CANCEL</Text>
                        </TouchableOpacity>
                    </View>
                </View>
            </Modal>
        );
    }

    renderEditButton() {
        if (this.state.isOnUpdate) {
            return (
                <TouchableOpacity onPress={() => this._toggleUpdate(1)}>
                    <Icon name='save' size={28} type='font-awesome' iconStyle={styles.iconContainer} />
                </TouchableOpacity>
            );
        }
        
        return (
            <TouchableOpacity onPress={() => this._toggleUpdate(0)}>
                <Icon name='pencil' type='simple-line-icon' iconStyle={styles.iconContainer} />
            </TouchableOpacity>
        );
    }

    renderName() {
        const WIDTH = Dimensions.get('window').width;

        if (!this.state.isLoading) {
            return (
                <TextInput
                    style={[styles.aboutNameText, { width: WIDTH - 40, textAlign: 'center' }]}
                    value={this.state.userName}
                    placeholder="Your Name"
                    underlineColorAndroid='transparent'
                    editable={this.state.isOnUpdate}
                    onChangeText={(userName) => this.setState({ userName })}
                />
            );
        }

        return (
            <Spinner
                size="small"
                color="red"
            />
        );
    }

    async _logOut() {
        await AsyncStorage.removeItem('logedIn');
        await AsyncStorage.removeItem('user');
        this.props.navigation.navigate('Main');
    }

    _navigateToLogin() {
        this.props.navigation.navigate('Login');
    }

    _navigateToMyStuff(path) {
        if (path === 'ads') {
            this.props.navigation.navigate('MyAds');            
        } else if (path === 'store') {
            this.props.navigation.navigate('MyStore');
        }
    }

    renderMyStuff() {
        if (this.state.logedIn) {
            return (
                <View style={styles.bottomButtons}>
                    <TouchableOpacity style={styles.bottomButtonContainer} onPress={this._navigateToMyStuff.bind(this, 'ads')}>
                        <Text style={styles.bottomButtonText}>MY ADS</Text>
                    </TouchableOpacity>
                    <TouchableOpacity style={styles.bottomButtonContainer} onPress={this._navigateToMyStuff.bind(this, 'store')}>
                        <Text style={styles.bottomButtonText}>MY STORE</Text>
                    </TouchableOpacity>
                </View>
            );
        }

        return null;
    }

    render() {
        const { 
            profileImageContainer,
            iconContainer,
            aboutContainer,
            aboutText,
            aboutDescription,
            formContainer,
            formItemContainer,
            formIconContainer,
            formIcon,
            formInputs,
            formInputText,
            bottomButtons,
            bottomButtonContainer,
            bottomButtonText
        } = styles;

        const WIDTH = Dimensions.get('window').width;

        const _renderLogout = () => {
            if (this.state.logedIn) {
                return (
                    <TouchableOpacity style={{ padding: 5 }} onPress={this._logOut.bind(this)}>
                        <Text style={{ color: '#fff', backgroundColor: 'transparent' }}>Logout</Text>
                    </TouchableOpacity>
                );
            } 
            
            return (
                <TouchableOpacity style={{ padding: 5 }} onPress={this._navigateToLogin.bind(this)}>
                    <Text style={{ color: '#fff', backgroundColor: 'transparent' }}>Login</Text>
                </TouchableOpacity>
            );
        };

        return (
            <ScrollView bounces={false} style={{ flex: 1, backgroundColor: '#fff' }}>
                <View style={{ flex: 1 }}>
                    <ImageBackground
                        style={{ height: 300, width: WIDTH }}
                        source={require('../assets/profile_bg_gradiant.png')}
                    >
                        <View style={{ alignItems: 'flex-end' }}>
                            {_renderLogout()}
                        </View>
                        <View style={{ flex: 1 }}>
                            <View style={profileImageContainer}>
                                <TouchableOpacity>
                                    <Icon name='bubbles' type='simple-line-icon' iconStyle={iconContainer} />
                                </TouchableOpacity>

                                <TouchableOpacity activeOpacity={!this.state.isOnUpdate ? 1 : 0.5} style={styles.pimageContainer} onPress={() => this.state.isOnUpdate && this.setImageSlectModalVisible(true)}>
                                    <Image
                                        style={{ flex: 1, borderRadius: 50 }}
                                        source={{ uri: (!this.state.image) ? 'http://via.placeholder.com/50x50' : this.state.image }}
                                    />
                                </TouchableOpacity>

                                {this.renderEditButton()}
                            </View>
                            <View style={aboutContainer}>
                                {this.renderName()}
                                <Text style={aboutText}>About me</Text>
                                <Text style={aboutDescription}>
                                    It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here', making it look like readable English.
                                </Text>
                            </View>
                        </View>
                    </ImageBackground>
                    <View style={formContainer}>
                        <View style={formItemContainer}>
                            <View style={formIconContainer}>
                                <Icon name='phone' type='font-awesome' iconStyle={formIcon} />
                            </View>
                            <View style={formInputs}>
                                <TextInput
                                    value={this.state.mobile}
                                    placeholder="+947xxxxxxx"
                                    keyboardType="phone-pad"                                 
                                    underlineColorAndroid='transparent'
                                    onChangeText={(mobile) => this.setState({ mobile })}
                                    editable={this.state.isOnUpdate}
                                />
                                <Text style={formInputText}>Mobile</Text>
                            </View>
                        </View>

                        <View style={formItemContainer}>
                            <View style={formIconContainer}>
                                <Icon name='email' type='material-community' iconStyle={formIcon} />
                            </View>
                            <View style={formInputs}>
                                <TextInput
                                    value={this.state.email}
                                    placeholder="jhon@example.com"
                                    underlineColorAndroid='transparent'
                                    editable={false}
                                />
                                <Text style={formInputText}>E-mail</Text>
                            </View>
                        </View>

                        <View style={formItemContainer}>
                            <View style={formIconContainer}>
                                <Icon name='location-on' iconStyle={formIcon} />
                            </View>
                            <View style={formInputs}>
                                <TextInput
                                    value={this.state.locationName}
                                    placeholder="Address"
                                    underlineColorAndroid='transparent'
                                    editable={this.state.isOnUpdate}
                                    onChangeText={(locationName) => this.setState({ locationName })}
                                />
                                <Text style={formInputText}>Address</Text>
                            </View>
                        </View>
                    </View>
                </View>
                {this.renderMyStuff()}
                {this.renderImageSelectionModal()}
            </ScrollView>
        );
    }
}

const styles = {
    headerContainer: {
        marginBottom: 80
    },
    profileImageContainer: {
        flexDirection: 'row',
        justifyContent: 'space-around',
        paddingTop: 20,
    },
    pimageContainer: {
        width: 100,
        height: 100,
        borderRadius: 50
    },
    iconContainer: {
        padding: 6,
        borderRadius: 19,
        borderWidth: 0.5,
        backgroundColor: '#ccc',  
        overflow: 'hidden',
    },
    aboutContainer: {
        alignItems: 'center',
        paddingTop: 5
    },
    aboutNameText: {
        color: '#fff',
        fontSize: 18,
        backgroundColor: 'transparent'
    },
    aboutText: {
        marginTop: 10,
        backgroundColor: 'transparent'
    },
    aboutDescription: {
        fontSize: 9,
        paddingLeft: 30,
        paddingRight: 30,
        textAlign: 'center',
        fontWeight: '300',
        color: '#3e3e3e',
        backgroundColor: 'transparent'
    },
    formContainer: {
        paddingRight: 10
    },
    formItemContainer: {
        flexDirection: 'row',
        height: 80,
        justifyContent: 'center',
    },
    formIconContainer: {
        justifyContent: 'center',
        paddingLeft: 20,
        paddingRight: 20
    },
    formIcon: {
        color: '#f50025'
    },
    formInputs: {
        flex: 1,
        justifyContent: 'center',
        borderBottomWidth: 0.5,
        borderColor: '#ddd'
    },
    formInputText: {
        fontSize: 10,
        fontWeight: '100',
        color: '#3e3e3e'
    },
    bottomButtons: {
        flexDirection: 'row'
    },
    bottomButtonContainer: {
        flex: 1,
        height: 60,
        borderWidth: 0.5,
        borderColor: '#fff',
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#f50025'
    },
    bottomButtonText: {
        color: '#fff'
    },
    imageSlectModal: {
        backgroundColor: '#fff',
        padding: 10,
        borderRadius: 5
    },
    imageSelectionButton: {
        backgroundColor: '#3ec2d7',
        marginBottom: 5,
        marginTop: 5,
        marginLeft: 10,
        marginRight: 10,
        padding: 15,
        justifyContent: 'center',
        alignItems: 'center'
    },
};

const mapStateToProps = (state) => ({
    nav: state.nav,
    tab: state.tab,
    settings: state.settings,
});

const mapDispatchToProps = (dispatch) => ({ ...bindActionCreators(ActionCreators, dispatch), dispatch });

export default connect(mapStateToProps, mapDispatchToProps)(ProfileScreen);
