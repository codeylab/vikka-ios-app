import React, { Component } from 'react';
import { 
    View, 
    Text,
    ScrollView,
    TouchableOpacity,
    Image,
    AsyncStorage,
    Dimensions,
    Modal
} from 'react-native';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import Icon from '@expo/vector-icons/FontAwesome';
import NewestIcon from '@expo/vector-icons/SimpleLineIcons';
import axios from 'axios';
import { Header, Icon as NewIcon } from 'react-native-elements';
import { NavigationActions } from 'react-navigation';
import * as ActionCreators from '../actions';

import HeaderText from '../components/headerText';
import Spinner from '../components/spinner';

class MyStoreScreen extends Component {
    constructor(props) {
        super(props);

        this.state = {
            data: null,
            isLoading: true,
            deleteModalVisible: false,
            storeId: null
        };
    }

    async componentDidMount() {
        await this.fetchStore();
    }

     _navigateBackToLogin() {
        this.props.navigation.goBack();
    }
    
    renderBackButton() {
        return (
            <TouchableOpacity onPress={() => this._navigateBackToLogin()}>
                <NewIcon name='arrow-back' color="white" />               
            </TouchableOpacity>
        );
    }

    setDeleteModalVisible(visible, id) {
        this.setState({
            deleteModalVisible: visible,
            storeId: id
        });
    }

    async fetchStore() {
        let user = await AsyncStorage.getItem('user');
        user = JSON.parse(user);
        if (user !== null) {
            user = user[0];
            const url = `http://vikka.mv/core/getMyStoreList.php?email=${user.uEmail_h}`;
            const { data } = await axios.get(url);
            this.setState({
                data: data.store,
                isLoading: false
            });
        }
    }

    async deleteStore() {
        const url = `http://vikka.mv/core/store_delete.php?st_id=${this.state.storeId}`;
        await axios.get(url);

        this.setState({
            isLoading: true,
            deleteModalVisible: false
        });

        await this.fetchStore();
    }

    _onPressButton(item) {
        this.props.dispatch(NavigationActions.navigate({ routeName: 'Store', params: { st_id: item.st_id } }));        
    }

    _navigateToCreate() {
        this.props.dispatch(NavigationActions.navigate({ routeName: 'CreateStore' }));                
    }

    renderDeleteModal() {
        const { imageSlectModal, imageSelectionButton } = styles;
        const WIDTH = Dimensions.get('window').width;

        return (
            <Modal
                animationType="fade"
                transparent
                visible={this.state.deleteModalVisible}
                onRequestClose={() => { this.setDeleteModalVisible(false, null); }}
            >
                <View 
                    style={{
                        flex: 1,
                        justifyContent: 'center',
                        alignItems: 'center',
                        backgroundColor: 'rgba(0,0,0,0.5)'
                    }}
                >
                    <View style={[imageSlectModal, { width: WIDTH - 20 }]}>
                        <View style={{ padding: 5, alignItems: 'center' }}>
                            <Text>Do you want to delete?</Text>
                        </View>

                        <TouchableOpacity style={imageSelectionButton} onPress={() => this.deleteStore()}>
                            <Text style={{ color: '#fff' }}>DELETE</Text>
                        </TouchableOpacity>

                        <TouchableOpacity onPress={() => this.setDeleteModalVisible(false, null)} style={[imageSelectionButton, { backgroundColor: '#ddd' }]}>
                            <Text style={{ color: '#fff' }}>CANCEL</Text>
                        </TouchableOpacity>
                    </View>
                </View>
            </Modal>
        );
    }

    gotoEdit(item) {
        this.props.dispatch(NavigationActions.navigate({ routeName: 'EditStore', params: { store: item } }));   
    }

    render() {
        const { 
            headerContainer,
            buttonContainer,
            buttonText
        } = styles;

        const renderList = () => {
            const {
                itemContainer,
                itemImageContainer,
                itemDetailContainer,
                itemTitle,
                itemDescription,
                itemLocation,
                itemCategory,
                itemPrice,
                itemImage,
            } = styles;

            if (this.state.data !== null && this.state.data.length > 0) {
                const listOfItems = this.state.data.map((item, index) => {
                    return (
                        <TouchableOpacity key={index} onPress={() => this._onPressButton(item)}>
                            <View style={itemContainer}>
                                <View style={itemImageContainer}>
                                    <Image
                                        style={[itemImage, { height: '100%' }]}
                                        source={{ uri: `http://www.vikka.mv/images/store/${item.st_logo}` }}
                                    />
                                </View>
                                <View style={itemDetailContainer}>
                                    <Text style={itemTitle}>{item.st_name}</Text>
                                    <Text style={itemDescription}>{item.st_desc}</Text>
                                    <View style={{ flexDirection: 'row', marginTop: 10, marginBottom: 10 }}>
                                        <Text style={itemLocation}>{item.st_loc}, </Text>
                                        <Text style={itemCategory}>{item.st_type}</Text>
                                    </View>
                                    <View style={{ flexDirection: 'row' }}>
                                        <Text style={[itemPrice, { alignSelf: 'flex-start' }]}>MVR {this.state.data[0].price} </Text>
                                        <View style={{ flex: 1, flexDirection: 'row', alignSelf: 'flex-end', justifyContent: 'flex-end', marginTop: -10 }}>
                                            <TouchableOpacity 
                                                style={{ paddingRight: 5 }}
                                                onPress={() => this.setDeleteModalVisible(true, item.st_id)}
                                            >
                                                <Icon name='trash-o' color="gray" size={25} />
                                            </TouchableOpacity>
                                            <TouchableOpacity 
                                                style={{ paddingTop: 3 }}
                                                onPress={() => this.gotoEdit(item)}
                                            >
                                                <NewestIcon name='pencil' color="gray" size={20} />
                                            </TouchableOpacity>
                                        </View>
                                    </View>
                                </View>
                            </View>
                        </TouchableOpacity>
                    );
                });
    
                return listOfItems;
            }

            return _emptyList();
        };

        const _emptyList = () => {
            if (this.state.isLoading) {
                return (
                    <Spinner
                        size="large"
                        color="red"
                    />
                )
            }
            return (
                <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center', marginTop: Dimensions.get('window').height / 5, paddingBottom: 10 }}>
                    <Image
                        source={{ uri: 'https://www.geartechnology.com/blog/wp-content/uploads/2015/11/norecord.png' }}
                        style={{ width: Dimensions.get('window').height / 2.9, height: Dimensions.get('window').height / 2.9 }}
                    />    
                </View>  
            );
        };

        return (
            <View style={{ flex: 1 }}>

                {this.renderDeleteModal()}
                <View style={headerContainer}>                
                    <Header
                        leftComponent={this.renderBackButton()}
                        centerComponent={<HeaderText title="My Stores" />}
                        outerContainerStyles={{ backgroundColor: '#f50025', height: 82 }}
                        innerContainerStyles={{ justifyContent: 'flex-start' }}
                    />
                </View>

                <ScrollView style={styles.scrollList}>
                    {renderList()}
                </ScrollView>
                
                <TouchableOpacity style={buttonContainer} onPress={this._navigateToCreate.bind(this)}>
                    <Text style={buttonText}>Create Store</Text>
                </TouchableOpacity>
            </View>
        );
    }
}

const styles = {
    scroll: {
        backgroundColor: '#e4e4e4'
    },
    headerContainer: {
        marginBottom: 80
    },
    itemTitleContainer: {
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#fff',
    },
    itemTitleText: {
        paddingTop: 10,
        paddingBottom: 10,
        fontSize: 16
    },
    itemPriceText: {
        paddingBottom: 10,
        fontSize: 18
    },
    buttonContainer: {
        backgroundColor: '#f50025',
        height: 60,
        alignItems: 'center',
        justifyContent: 'center'
    },
    buttonText: {
        color: '#fff'
    },
    scrollList: {
        paddingLeft: 10,
        paddingRight: 10
    },
    imageContainer: {
        flex: 1,
        backgroundColor: '#fff'
    },
    listContainer: {
        flex: 2,
        backgroundColor: '#fff'
    },
    companyDetailsContainer: {
        padding: 10,
        paddingTop: 5,
        paddingBottom: 5,
    },
    coverImage: {
        flex: 2
    },
    profileImage: {
        flex: 1,
        borderWidth: 2,
        borderRadius: 2,
        borderColor: '#fff',
        shadowColor: '#000',
        shadowOpacity: 0.75,
        shadowOffset: { height: 0, width: 0 },
        position: 'relative',
        marginLeft: 10
    },
    companyName: {
        position: 'relative',
        fontWeight: 'bold',
    },
    companyAddress: {
        fontSize: 12,
        color: '#808080'
    },
    companyDrescription: {
        fontSize: 12,
        marginTop: 5
    },
    companyWebsite: {
        fontSize: 12,
        color: '#06a9d1'
    },
    companyContact: {
        fontSize: 12   
    },
    openHours: {
        fontSize: 12        
    },
    storeType: {
        marginTop: 10,
        fontWeight: 'bold',
        color: '#808080'
    },
    itemContainer: {
        flex: 1,
        flexDirection: 'row',
        borderColor: '#ddd',
        borderTopWidth: 0.5,
        borderBottomWidth: 0.5,
        paddingTop: 10,
        paddingBottom: 10
    },
    itemImageContainer: {
        flex: 1,
        borderRadius: 2,
        borderWidth: 0.5,
        borderColor: '#000'
    },
    itemDetailContainer: {
        flex: 3,
        paddingLeft: 15  
    },
    itemTitle: {

    },
    itemDescription: {
        color: '#808080',
        fontSize: 10
    },
    itemLocation: {
        color: '#808080',
    },
    itemCategory: {
        color: '#808080',
    },
    itemPrice: {
        
    },
    itemImage: {
        flex: 1,
        borderWidth: 2,
        borderRadius: 2,
        borderColor: '#fff',
        shadowColor: 'red',
        shadowOpacity: 0.5,
        shadowOffset: { height: 2, width: 2 },
    },
    imageSlectModal: {
        backgroundColor: '#fff',
        padding: 10,
        borderRadius: 5
    },
    imageSelectionButton: {
        backgroundColor: '#3ec2d7',
        marginBottom: 5,
        marginTop: 5,
        marginLeft: 10,
        marginRight: 10,
        padding: 15,
        justifyContent: 'center',
        alignItems: 'center'
    },
};


const mapStateToProps = (state) => ({
    nav: state.nav,
    settings: state.settings
});

const mapDispatchToProps = (dispatch) => ({ ...bindActionCreators(ActionCreators, dispatch), dispatch });

export default connect(mapStateToProps, mapDispatchToProps)(MyStoreScreen);
