import React, { Component } from 'react';
import { 
    View, 
    Text,
    StatusBar,
    TouchableOpacity,
    KeyboardAvoidingView,
} from 'react-native';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { NavigationActions } from 'react-navigation';

import * as ActionCreators from '../actions';

import LoginInputText from '../components/loginInputText';
import LoginButton from '../components/loginButton';

class ForgotPassScreen extends Component {
    constructor(props) {
        super(props);

        this.state = {
            data: []
        };
    }

    _navigateBackToLogin() {
        this.props.dispatch(NavigationActions.navigate({ routeName: 'Login' }));
    }

    _sendForgotPasswordEmail() {

    }

    render() {
        const { mainContainer, 
            container, 
            welcomeTextContainer, 
            welcomeText, 
            loginTextContainer, 
            loginText, 
            singUpTextContainer
        } = styles;

        return (
            <KeyboardAvoidingView 
                behavior='padding'
				style={mainContainer}
            >
                <StatusBar hidden />

                <View style={container}>
                    <View style={welcomeTextContainer}>
                        <Text style={welcomeText}>Forgot Password</Text>
                    </View>

                    <LoginInputText 
                        autoCapitalize={'none'}
                        returnKeyType={'done'}
                        placeholder="Email Address"
                        autoCorrect={false}
                        topRound
                        bottomRound
                    />

                    <LoginButton onPressBtn={() => this._sendForgotPasswordEmail()} buttonText="SEND ME MY PASSWORD" />

                </View>

                    <View style={[loginTextContainer, singUpTextContainer]}>
                        <TouchableOpacity onPress={() => this._navigateBackToLogin()}>
                            <Text style={loginText}>Back to Login</Text>
                        </TouchableOpacity>
                    </View>
            </KeyboardAvoidingView>
        );
    }
}

const styles = {
    mainContainer: {
        flex: 1,
        backgroundColor: '#820d2a'
    },
    container: {
        flex: 1,
        justifyContent: 'center',
    },
    welcomeTextContainer: {
        alignItems: 'center',
        paddingBottom: 40
    },
    welcomeText: {
        color: 'white',
        fontSize: 32,
        fontWeight: 'bold'
    },
    loginTextContainer: {
        alignItems: 'center',
        marginTop: 10
    },
    loginText: {
        color: 'white',
        fontSize: 16
    },
    singUpTextContainer: {
        marginBottom: 40
    }
};


const mapStateToProps = (state) => ({
    nav: state.nav,
    settings: state.settings
});

const mapDispatchToProps = (dispatch) => ({ ...bindActionCreators(ActionCreators, dispatch), dispatch });

export default connect(mapStateToProps, mapDispatchToProps)(ForgotPassScreen);
