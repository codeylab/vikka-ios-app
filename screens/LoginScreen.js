import React, { Component } from 'react';
import { 
    View, 
    Text,
    StatusBar,
    TouchableOpacity,
    KeyboardAvoidingView,
    AsyncStorage
} from 'react-native';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { NavigationActions } from 'react-navigation';
import axios from 'axios';
import base64 from 'base-64';

import * as ActionCreators from '../actions';

import LoginInputText from '../components/loginInputText';
import LoginButton from '../components/loginButton';
import Spinner from '../components/spinner';

class LoginScreen extends Component {
    constructor(props) {
        super(props);

        this.state = {
            data: [],
            loading: false,
            email: '',
            password: '',
            error: false
        };
    }

    _navigateToForgotPassword() {
        this.props.dispatch(NavigationActions.navigate({ routeName: 'PassForgot' }));
    }

    _navigateToSingUp() {
        this.props.dispatch(NavigationActions.navigate({ routeName: 'Register' }));
    }

    async _navigateToMain() {
        this.setState({
            loading: true
        });

        const {
            email, password
        } = this.state;

        try {
            const encodedPassword = base64.encode(password);
            const url = `http://vikka.mv/core/user_login.php?emid=${email}&pws=${encodedPassword}`;
            const userUrl = `http://vikka.mv/core/user_profile.php?user_email=${email}`;
            const { data } = await axios.get(url);
            if (data.query_status[0].q_status === 1) {
                const userData = await axios.get(userUrl);

                await AsyncStorage.setItem('logedIn', JSON.stringify(true));
                
                if (userData.data.user_profile !== null) {
                    await AsyncStorage.setItem('user', JSON.stringify(userData.data.user_profile));                    
                }
                
                this.setState({
                    loading: false
                });
                this.props.dispatch(NavigationActions.navigate({ routeName: 'Main' }));  
            } else {
                this.setState({
                    error: true,
                    loading: false
                });
            }
        } catch (e) {
            this.setState({
                error: true,
                loading: false
            });
        }
    }

    _emailChange(text) {
        this.setState({
            email: text
        });
    }

    _passwordChange(text) {
        this.setState({
            password: text
        });
    }

    renderButtonOrSpinner() {
        if (this.state.loading) {
            return (
                <View style={styles.spinner}>
                    <Spinner
                        size="large"
                        color="white"
                    />
                </View>
            );
        }
        return (
            <LoginButton onPressBtn={() => this._navigateToMain()} buttonText="LOGIN" />
        );
    }

    render() {
        const { mainContainer, 
            container, 
            welcomeTextContainer, 
            welcomeText, 
            loginTextContainer, 
            loginText, 
            singUpTextContainer,
            errorText
        } = styles;

        const showError = () => {
            if (this.state.error) {
                return (
                    <View style={loginTextContainer}>
                        <TouchableOpacity onPress={() => this._navigateToForgotPassword()}>
                            <Text style={[loginText, errorText]}>Login failed!</Text>
                        </TouchableOpacity>
                    </View>
                );
            }

            return null;
        };

        return (
            <KeyboardAvoidingView 
                behavior='padding'
				style={mainContainer}
            >
                <StatusBar hidden />

                <View style={container}>
                    <View style={welcomeTextContainer}>
                        <Text style={welcomeText}>Welcome!</Text>
                    </View>

                    <LoginInputText 
                        autoCapitalize={'none'}
                        returnKeyType={'done'}
                        placeholder="Email Address"
                        autoCorrect={false}
                        topRound 
                        value={this.state.email}
                        onChangeText={this._emailChange.bind(this)}
                    />

                    <LoginInputText 
                        autoCapitalize={'none'}
                        returnKeyType={'done'}
                        placeholder="Password"
                        autoCorrect={false}
                        bottomRound
                        secureTextEntry
                        value={this.state.password}
                        onChangeText={this._passwordChange.bind(this)}
                    />

                    {this.renderButtonOrSpinner()}

                    <View style={loginTextContainer}>
                        <TouchableOpacity onPress={() => this._navigateToForgotPassword()}>
                            <Text style={loginText}>Forgot Password?</Text>
                        </TouchableOpacity>
                    </View>

                    {showError()}

                </View>

                    <View style={[loginTextContainer, singUpTextContainer]}>
                        <TouchableOpacity onPress={() => this._navigateToSingUp()}>
                            <Text style={loginText}>Don't have account? Sing Up</Text>
                        </TouchableOpacity>
                    </View>
            </KeyboardAvoidingView>
        );
    }
}

const styles = {
    mainContainer: {
        flex: 1,
        backgroundColor: '#820d2a'
    },
    container: {
        flex: 1,
        justifyContent: 'center',
    },
    welcomeTextContainer: {
        alignItems: 'center',
        paddingBottom: 40
    },
    welcomeText: {
        color: 'white',
        fontSize: 32,
        fontWeight: 'bold'
    },
    loginTextContainer: {
        alignItems: 'center',
        marginTop: 10
    },
    loginText: {
        color: 'white',
        fontSize: 16
    },
    singUpTextContainer: {
        marginBottom: 40
    },
    spinner: {
        marginTop: 30,
        paddingLeft: 20,
        paddingRight: 20,
    },
    errorText: {
        color: 'orange'
    }
};


const mapStateToProps = (state) => ({
    nav: state.nav,
    settings: state.settings
});

const mapDispatchToProps = (dispatch) => ({ ...bindActionCreators(ActionCreators, dispatch), dispatch });

export default connect(mapStateToProps, mapDispatchToProps)(LoginScreen);
