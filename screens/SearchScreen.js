import React, { Component } from 'react';
import {
    View,
    Text,
    TouchableWithoutFeedback,
    TouchableOpacity,
    Image,
    ScrollView,
    AsyncStorage,
    Dimensions,
    FlatList
} from 'react-native';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { SearchBar } from 'react-native-elements';
import ImageSlider from 'react-native-image-slider';
import { NavigationActions } from 'react-navigation';
import axios from 'axios';
import Icon from '@expo/vector-icons/Octicons';

import * as ActionCreators from '../actions';
import LocationSelector from '../components/locationSelector';
import CategorySelector from '../components/categorySelector';
import SortSelector from '../components/sortSelector';
import Spinner from '../components/spinner';
import ItemListOfStore from '../components/itemListOfStore';


const imageSliderHeight = 150;
const viewerHeight = 60;
const headerHeight = 40;

const styles = {
    headerStyle: {
        backgroundColor: 'red',
        height: 55,
    },
    inputStyle: {
        height: 35,
        color: 'black',
        backgroundColor: 'white',
    },
    containerStyle: {
        width: 270,
        height: headerHeight,
        marginRight: 10,
        backgroundColor: 'red',
        borderTopWidth: 0,
        borderBottomWidth: 0,
        marginTop: -10
    },
    mainView: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        marginTop: 15,
        paddingBottom: 0,
        marginBottom: 0,
        height: viewerHeight,
    },
    subView: {
        flex: 1,
        backgroundColor: 'red',
        borderRightWidth: 1,
        borderRightColor: '#FFF',
        justifyContent: 'center',
        alignItems: 'center',
    },
    lastSubView: {
        borderRightWidth: 0
    },
    viewTextStyle: {
        textAlign: 'center',
        color: '#FFF',
        fontWeight: '700',
        fontSize: 12,
        marginRight: 5
    },
    smallText: {
        fontWeight: 'normal',
        fontSize: 10,
        textAlign: 'left',
    },
    lastSmallText: {
        marginLeft: '23%',
    },
    imageContainer: {
        flex: 1,
        backgroundColor: '#fff'
    },
    listContainer: {
        flex: 2,
        backgroundColor: '#fff'
    },
    companyDetailsContainer: {
        padding: 10,
        paddingTop: 5,
        paddingBottom: 5,
    },
    coverImage: {
        flex: 2
    },
    profileImage: {
        flex: 1,
        borderWidth: 2,
        borderRadius: 2,
        borderColor: '#fff',
        shadowColor: '#000',
        shadowOpacity: 0.75,
        shadowOffset: { height: 0, width: 0 },
        position: 'relative',
        marginLeft: 10
    },
    companyName: {
        position: 'relative',
        fontWeight: 'bold',
    },
    companyAddress: {
        fontSize: 12,
        color: '#808080'
    },
    companyDrescription: {
        fontSize: 12,
        marginTop: 5
    },
    companyWebsite: {
        fontSize: 12,
        color: '#06a9d1'
    },
    companyContact: {
        fontSize: 12   
    },
    openHours: {
        fontSize: 12        
    },
    storeType: {
        marginTop: 10,
        fontWeight: 'bold',
        color: '#808080'
    },
    scrollList: {
        marginTop: 0,
        marginBottom: imageSliderHeight + viewerHeight,
        height: Dimensions.get('window').height - (imageSliderHeight + viewerHeight + headerHeight + 30)
    },
    itemContainer: {
        flex: 1,
        flexDirection: 'row',
        borderColor: '#ddd',
        borderTopWidth: 0.5,
        borderBottomWidth: 0.5,
        paddingTop: 10,
        paddingBottom: 10
    },
    itemImageContainer: {
        flex: 1,
        borderRadius: 2,
        borderWidth: 0.5,
        borderColor: '#000',
        height: 75
    },
    itemDetailContainer: {
        flex: 3,
        paddingLeft: 15  
    },
    itemDescription: {
        color: '#808080',
        fontSize: 10
    },
    itemLocation: {
        color: '#808080',
    },
    itemCategory: {
        color: '#808080',
    },
    itemImage: {
        flex: 1,
        borderWidth: 2,
        borderRadius: 2,
        borderColor: '#fff',
        shadowColor: 'red',
        shadowOpacity: 0.5,
        shadowOffset: { height: 2, width: 2 },
    },
};

class HeaderSearchBar extends Component {
    constructor(props) {
        super(props);

        this.state = {
            text: (props.value) ? props.value : '',
        };
    }

    changeText(text) {
        this.setState({
            text
        });
    }

    clearText({ text }) {
        this.setState({
            text
        });
    }

    render() {
        return (
            <SearchBar 
                containerStyle={styles.containerStyle}
                inputStyle={styles.inputStyle}
                value={this.state.text}
                onChangeText={this.changeText.bind(this)}
                onClearText={this.changeText.bind(this)}
            />
        );
    }
}

HeaderSearchBar.defaultProps = {
    value: '',
};

class SearchScreen extends Component {
    static navigationOptions = ({ navigation }) => ({
        headerStyle: styles.headerStyle,
        headerTintColor: '#FFF',        
        headerRight: (<HeaderSearchBar value={(typeof navigation.state.params !== 'undefined') ? navigation.state.params.text : ''} />)
    });

    constructor(props) {
        super(props);
        
        this.state = {
            position: 0,
            interval: null,
            isLoading: true,
            locationVisible: false,            
            data: null,
        };
    }

    componentWillMount() {
        this.setState({ interval: setInterval(() => {
            this.setState({ position: this.state.position === 3 ? 0 : this.state.position + 1 });
        }, 10000) });
    }

    async componentDidMount() {
        let location = '';
        let category = '';

        if (typeof this.props.navigation.state.params.item !== 'undefined') {
            category = this.props.navigation.state.params.item;            
            this.props.setCategory(category);     
            this._fetchData('', category, this.props.store.sort.value);            
        } else {
            location = await AsyncStorage.getItem('location');
            if (location != null) {
                location = JSON.parse(location);   
                this.props.setLocation(location);                
            } else {
                location = {};
                location.city_id = '';
            } 
            
            category = await AsyncStorage.getItem('category');
            if (category != null) {
                category = JSON.parse(category);     
                this.props.setCategory(category);                
            } else {
                category = {};
                category.child_id = '';
            }
            await this._fetchData(location.city_id, category, this.props.store.sort.value);            
        }   
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.store.location != null && nextProps.store.category != null && nextProps.store.category != '') {
            if (nextProps.store.location.city_id !== this.props.store.location.city_id || nextProps.store.category.city_id !== this.props.store.category.child_id) {
                this.setState({
                    data: null,
                    isLoading: true
                });
                this._fetchData(nextProps.store.location.city_id, nextProps.store.category, nextProps.store.sort.value);                                
            }
        }
    }

    shouldComponentUpdate(nextProps, nextState) {
        if (nextProps.store.location != '' || nextProps.store.category != '') {
           return true;              
        }

        return false;
    }

    componentWillUnmount() {
        this.setState({
            data: null
        });

        clearInterval(this.state.interval);
    }

    async _fetchData(locationId = '', category = '', sort = '') {
        let url = 'http://vikka.mv/core/app_list_view.php';    
        
        if (locationId !== '') {
            url = `${url}?loc_id=${locationId}`;
        }

        if (category !== '') {
            const mark = (locationId === '') ? '?' : '&';
            if (typeof category.child_id === 'undefined' && typeof category.id != 'undefined') {
                url = `${url}${mark}main_cat_id=${category.id}`;                            
            } else if (typeof category.main_id != 'undefined' && typeof category.child_id != 'undefined' && category.child_id != '') {
                url = `${url}${mark}main_cat_id=${category.main_id}&sub_cat_id=${category.child_id}`;                            
            } 
        }

        if (sort !== '') {
            const mark = ((typeof category.child_id !== 'undefined' && typeof category.id != 'undefined' && typeof category.main_id != 'undefined' && category.child_id != '') || locationId !== '') ? '&' : '?';
            url = `${url}${mark}sort_code=${sort}`;
        }

        try {
            const data = await axios.get(url);  
            this.setState({
                isLoading: false,            
                data: data.data.list_view,
            });          
        } catch (e) {
            this.setState({
                isLoading: false,            
            });  
        }
    }

    _goTo(type) {
        if (type === 'location') {
            this.props.showLocationModal(true);
        } else if (type === 'category') {
            this.props.showCategoryModal(true);
        } else if (type === 'sort') {
            this.props.showSortModal(true);
        }
    }

    _onPressButton(item) {
        this.props.dispatch(NavigationActions.navigate({ 
            routeName: 'Item', 
            params: {
                data: item
            }
        }));
    }

    _setScroll(status) {
        this.props.setScroll(status);            
    }


    render() {
        const {
            scrollList
         } = styles;
        
        const selectedLocation = (this.props.store.location !== '') ? this.props.store.location.city_name : '';
        const selectedCategory = (this.props.store.category !== '') ? this.props.store.category : '';        

        return (
            <View>
                <View>
                    <ImageSlider
                        height={imageSliderHeight}
                        images={[
                            'http://placeimg.com/640/480/any',
                            'http://placeimg.com/640/480/any',
                            'http://placeimg.com/640/480/any',
                            'https://images.pexels.com/photos/216355/pexels-photo-216355.jpeg?w=940&h=650&auto=compress&cs=tinysrgb',
                        ]} 
                        position={this.state.position}
                        onPositionChanged={position => this.setState({ position })} 
                    />
                </View>

                <View style={styles.mainView}>
                    <TouchableWithoutFeedback 
                        onPress={this._goTo.bind(this, 'location')}
                    >
                        <View style={[styles.subView]}>
                            <View style={{ flexDirection: 'row' }}>
                                <Text style={styles.viewTextStyle}>LOCATION</Text>
                                <Icon name='triangle-down' color='#FFF' size={16} />
                            </View>
                            <Text style={[styles.viewTextStyle, styles.smallText]}>
                                {selectedLocation}
                            </Text>
                        </View>
                    </TouchableWithoutFeedback>
                    
                    <TouchableWithoutFeedback 
                        onPress={this._goTo.bind(this, 'category')}
                    >
                    <View style={[styles.subView]}>
                        <View style={{ flexDirection: 'row' }}>
                            <Text style={styles.viewTextStyle}>CATEGORY</Text>
                            <Icon name='triangle-down' color='#FFF' size={16} />
                        </View>
                        <Text style={[styles.viewTextStyle, styles.smallText]}>
                            {(typeof selectedCategory.child_name === 'undefined' ? selectedCategory.name : selectedCategory.child_name)}
                        </Text>
                    </View>
                    </TouchableWithoutFeedback>
                    
                    <TouchableWithoutFeedback 
                        onPress={this._goTo.bind(this, 'sort')}
                    >
                    <View style={[styles.subView, styles.lastSubView]}>
                        <View style={{ flexDirection: 'row' }}>
                            <Text style={styles.viewTextStyle}>SORT</Text>
                            <Icon name='triangle-down' color='#FFF' size={16} />
                        </View>
                        <Text style={[styles.viewTextStyle, styles.smallText]}>
                            {this.props.store.sort.name}
                        </Text>
                    </View>
                    </TouchableWithoutFeedback>
                </View>

                {!this.state.isLoading &&
                <View style={{ paddingLeft: 10, paddingRight: 10, backgroundColor: '#fff' }}>
                <FlatList
                    onTouchStart={this._setScroll.bind(this, false)}
                    onMomentumScrollEnd={this._setScroll.bind(this, true)}
                    onScrollEndDrag={this._setScroll.bind(this, true)}
                    showsVerticalScrollIndicator={false}
                    style={scrollList}
                    data={this.state.data}
                    keyExtractor={({ ls_ad_id }) => ls_ad_id}
                    renderItem={({ item, index }) => (<ItemListOfStore item={item} key={index} />)}
                    numColumns={1}
                    initialNumToRender={10}
                />
                </View>}

                {this.state.isLoading && <View style={{ justifyContent: 'center', padding: 100 }}>
                    <Spinner
                        size="large"
                        color="red"
                    />
                </View>}

                <LocationSelector showModal={this.props.store.showLocationModal} />
                <CategorySelector showModal={this.props.store.showCategoryModal} />
                <SortSelector showModal={this.props.store.showSortModal} />
            </View>
        );
    }
}

const mapStateToProps = (state) => ({
    nav: state.nav,
    tab: state.tab,
    settings: state.settings,
    store: state.store
});

const mapDispatchToProps = (dispatch) => ({ ...bindActionCreators(ActionCreators, dispatch), dispatch });

export default connect(mapStateToProps, mapDispatchToProps)(SearchScreen);
