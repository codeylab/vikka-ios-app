import React, { Component } from 'react';
import { Button, Image, View, Alert } from 'react-native';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { ImagePicker } from 'expo';

import * as ActionCreators from '../actions';

class DemoScreen extends Component {
    constructor(props) {
        super(props);

        this.state = {
             image: null,
        };
    }

    _pickImage = async () => {
        const { Permissions } = Expo; 
        const permissions = Permissions.CAMERA;
        const status = await Permissions.askAsync(permissions);

        if (status.status !== 'granted') {
            Alert.alert('Permission Rejected', 'Sorry, We can\'t access the camera');
        } else {
            const result = await ImagePicker.launchImageLibraryAsync({
            allowsEditing: false,
            aspect: [4, 3],
            });
    
            if (!result.cancelled) {
                this.setState({ image: result.uri });
            }     
        }

        
    };

    _takePhoto = async () => {
        const pickerResult = await ImagePicker.launchCameraAsync({
        allowsEditing: false,
        aspect: [4, 3],
        });

        if (!pickerResult.cancelled) {
            this.setState({ image: pickerResult.uri });
        }
    };


    render() {
        const { image } = this.state;
        
        return (
            <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
                <Button
                title="Pick an image from camera roll"
                onPress={this._takePhoto}
                />
                {image &&
                <Image source={{ uri: image }} style={{ width: 200, height: 200 }} />}
            </View>
        );
    }
}

const mapStateToProps = (state) => ({
    nav: state.nav,
    settings: state.settings
});

const mapDispatchToProps = (dispatch) => ({ ...bindActionCreators(ActionCreators, dispatch), dispatch });

export default connect(mapStateToProps, mapDispatchToProps)(DemoScreen);
