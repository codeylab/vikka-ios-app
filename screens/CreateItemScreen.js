import React, { Component } from 'react';
import {  
    View,
    Text, 
    Image,
    TouchableOpacity,
    ScrollView,
    TextInput,
    Modal,
    Dimensions,
    AsyncStorage,
    Alert
} from 'react-native';
import { Header, Icon } from 'react-native-elements';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { ImagePicker, Permissions } from 'expo';
import axios from 'axios';
import { NavigationActions } from 'react-navigation';

import * as ActionCreators from '../actions';

import Panel from '../components/Panel';
import HeaderText from '../components/headerText';
import LocationSelector from '../components/locationSelector';
import CategorySelector from '../components/categorySelector';
import StoreSelector from '../components/storeSelector';
import Spinner from '../components/spinner';

class StroeScreen extends Component {
    constructor(props) {
        super(props);

        this.state = {
            view2LayoutProps: {
                width: 0,
                height: 0
            },
            placeOfModal: null,
            imageSlectModal: false,
            image1: null,
            image2: null,
            image3: null,
            image4: null,
            isLoading: false,
            modalStoreVisible: false,
            storeType: 'Store Type',
            storeTypeValue: 0,
            modalLocationVisible: false,
            locationName: 'Location',
            locationValue: 0,
            modalCategoryVisible: false,
            categoryName: 'Category',
            categoryValue: 0,
            itemName: '',
            price: '',
            contactNo: '',
            description: '',
            storeData: [
                {
                    id: 1,
                    name: 'Retailer'
                },
                {
                    id: 2,
                    name: 'Wholesaler'
                }
            ],
        };

        this.createItem = this.createItem.bind(this);
    }

    onLayout(event) {
        const { height, width } = event.nativeEvent.layout;

        this.setState({ height, width });
    }

    setLocationModalVisible() {
        this.props.showLocationModal(true);
    }

    setStoreModalVisible() {
        this.props.showStoreModal(true);
    }

    setCategoryModalVisible() {
        this.props.showCategoryModal(true);
    }
    setimageSlectModalVisible(visible, place) {
        this.setState({ imageSlectModal: visible, placeOfModal: place });
    }

    _itemNameChange(text) {
        this.setState({
            itemName: text
        });
    }

    _priceChange(text) {
        this.setState({
            price: text
        });
    }

    _contactNoChange(text) {
        this.setState({
            contactNo: text
        });
    }

    _descriptionChange(text) {
        this.setState({
            description: text
        });
    }


    async _createItem() {
       
        const { itemName, price, contactNo, description, image1, image2, image3, image4 } = this.state;
        const { dispatch } = this.props;
        const { location, category, store } = this.props.store;

        this.setState({
            isLoading: true
        });

        let isLogedIn = await AsyncStorage.getItem('logedIn');
        let user = await AsyncStorage.getItem('user');

        isLogedIn = JSON.parse(isLogedIn);
        user = JSON.parse(user)[0];  

        if (isLogedIn && location !== '' && category !== '' && store !== '' && itemName !== '' && price !== '' && description !== '' && contactNo !== '') {
            const url = `http://vikka.mv/core/app_ad_upload.php?main_cat_id=${category.main_id}&sub_cat_id=${category.child_id}&loc_id=${location.city_id}&ad_price=${price}&ad_title=${itemName}&login_id=1&ad_desc=${description}&ad_store_id=${store.st_id}&ad_contact=${contactNo}&user_email=${user.uEmail_h}&st_sql_stat=i`;
            try {
                const { data } = await axios.get(url);

                if (data.query_status[0].q_status === '1') {
                    const adId = data.query_status[0].ad_id;
                    
                    if (image1) {
                        let image1Name = image1.split('/').pop();
                        image1Name = `${new Date().getTime()}.${image1Name.split('.').pop()}`;

                        const matchProfile = /\.(\w+)$/.exec(image1Name);
                        const image1Type = matchProfile ? `image/${matchProfile[1]}` : 'image';

                        const imgURL = `http://vikka.mv/core/app_ad_image_upload.php?ad_id=${adId}&email_id=${user.uEmail_h}&img_path=${image1Name}&im_count=0&st_sql_stat=u`;
                        
                        const image1FormData = new FormData();
                        image1FormData.append('filUpload', { uri: image1, name: image1Name, type: image1Type });

                        await axios.post(imgURL, image1FormData, { headers: { 'Content-Type': 'multipart/form-data' } }); 
                    }

                    if (image2) {
                        let image1Name = image2.split('/').pop();
                        image1Name = `${new Date().getTime()}.${image1Name.split('.').pop()}`;

                        const matchProfile = /\.(\w+)$/.exec(image1Name);
                        const image1Type = matchProfile ? `image/${matchProfile[1]}` : 'image';

                        const imgURL = `http://vikka.mv/core/app_ad_image_upload.php?ad_id=${adId}&email_id=${user.uEmail_h}&img_path=${image1Name}&im_count=1&st_sql_stat=u`;
                        
                        const image1FormData = new FormData();
                        image1FormData.append('filUpload', { uri: image2, name: image1Name, type: image1Type });

                        await axios.post(imgURL, image1FormData, { headers: { 'Content-Type': 'multipart/form-data' } }); 
                    }

                    if (image3) {
                        let image1Name = image3.split('/').pop();
                        image1Name = `${new Date().getTime()}.${image1Name.split('.').pop()}`;

                        const matchProfile = /\.(\w+)$/.exec(image1Name);
                        const image1Type = matchProfile ? `image/${matchProfile[1]}` : 'image';

                        const imgURL = `http://vikka.mv/core/app_ad_image_upload.php?ad_id=${adId}&email_id=${user.uEmail_h}&img_path=${image1Name}&im_count=2&st_sql_stat=u`;
                        
                        const image1FormData = new FormData();
                        image1FormData.append('filUpload', { uri: image3, name: image1Name, type: image1Type });

                        await axios.post(imgURL, image1FormData, { headers: { 'Content-Type': 'multipart/form-data' } }); 
                    }

                    if (image4) {
                        let image1Name = image4.split('/').pop();
                        image1Name = `${new Date().getTime()}.${image1Name.split('.').pop()}`;

                        const matchProfile = /\.(\w+)$/.exec(image1Name);
                        const image1Type = matchProfile ? `image/${matchProfile[1]}` : 'image';

                        const imgURL = `http://vikka.mv/core/app_ad_image_upload.php?ad_id=${adId}&email_id=${user.uEmail_h}&img_path=${image1Name}&im_count=3&st_sql_stat=u`;
                        
                        const image1FormData = new FormData();
                        image1FormData.append('filUpload', { uri: image4, name: image1Name, type: image1Type });

                        await axios.post(imgURL, image1FormData, { headers: { 'Content-Type': 'multipart/form-data' } }); 
                    }
                    
                    this.setState({
                        isLoading: false
                    });

                    dispatch(NavigationActions.navigate({ routeName: 'MyAds' }));
                }
            } catch (e) {
            }
        }        
    }

    _postButtonClick() {
        
    }

    _navigateBackToStoreList() {
        this.props.navigation.goBack();
    }

    _pickImage = async (place) => {
        const result = await ImagePicker.launchImageLibraryAsync({
            allowsEditing: false,
            aspect: [4, 3],
        }); 
    
        if (!result.cancelled) {
            if (place === 1) {
                this.setState({ image1: result.uri });
            }
            if (place === 2) {
                this.setState({ image2: result.uri });
            }
            if (place === 3) {
                this.setState({ image3: result.uri });
            }
            if (place === 4) {
                this.setState({ image4: result.uri });
            }
        }

        this.setimageSlectModalVisible(false, null);
    };

    _takePhoto = async (place) => {
        const { Permissions } = Expo; 
        const permissions = Permissions.CAMERA;
        const status = await Permissions.askAsync(permissions);

        if (status.status !== 'granted') {
            this.setimageSlectModalVisible(false, null);    
            Alert.alert('Permission Rejected', 'Sorry, We can\'t access the camera');
        } else {
            const result = await ImagePicker.launchCameraAsync({
                allowsEditing: false,
                aspect: [4, 3],
            });
    
            if (!result.cancelled) {
                if (place === 1) {
                    this.setState({ image1: result.uri });
                    
                }
                if (place === 2) {
                    this.setState({ image2: result.uri });
                }
                if (place === 3) {
                    this.setState({ image3: result.uri });
                }
                if (place === 4) {
                    this.setState({ image4: result.uri });
                }
            }

            this.setimageSlectModalVisible(false, null);      
        }
         
    };

    _removeImage = async (place) => {
        if (place === 1) {
            this.setState({ image1: null });
        }
        if (place === 2) {
            this.setState({ image2: null });
        }
        if (place === 3) {
            this.setState({ image3: null });
        }
        if (place === 4) {
            this.setState({ image4: null });
        }
        
        this.setimageSlectModalVisible(false, null);        
    }

    _renderImage(img, place) {
        if (img) {
            return (
                <TouchableOpacity style={{ flex: 1 }} onPress={() => { this.setimageSlectModalVisible(true, place); }}>
                    <Image
                        style={styles.itemImage}
                        source={{ uri: img }}
                    />
                </TouchableOpacity>
            );
        }
        return (
            <TouchableOpacity style={{ flex: 1 }} onPress={() => { this.setimageSlectModalVisible(true, place); }}>
                <Image
                    style={styles.itemImage}
                    source={require('../assets/image_ad_post.png')}
                />
            </TouchableOpacity>
        );
    }

    createItem() {

    }

    renderBackButton() {
        return (
            <TouchableOpacity onPress={() => this._navigateBackToStoreList()}>
                <Icon name='arrow-back' color="white" />               
            </TouchableOpacity>
        );
    }

    renderImageSelectionModal() {
        const { imageSlectModal, imageSelectionButton } = styles;
        const WIDTH = Dimensions.get('window').width;

        return (
            <Modal
                animationType="fade"
                transparent
                visible={this.state.imageSlectModal}
                onRequestClose={() => { this.setimageSlectModalVisible(false); }}
            >
                <View 
                    style={{
                        flex: 1,
                        justifyContent: 'center',
                        alignItems: 'center',
                        backgroundColor: 'rgba(0,0,0,0.5)'
                    }}
                >
                    <View style={[imageSlectModal, { width: WIDTH - 20 }]}>
                        <View style={{ padding: 5, alignItems: 'center' }}>
                            <Text>Choose a photo</Text>
                        </View>
                        <TouchableOpacity style={imageSelectionButton} onPress={() => this._takePhoto(this.state.placeOfModal)}>
                            <Text style={{ color: '#fff' }}>FROM CAMERA</Text>
                        </TouchableOpacity>

                        <TouchableOpacity style={imageSelectionButton} onPress={() => this._pickImage(this.state.placeOfModal)}>
                            <Text style={{ color: '#fff' }}>FROM GALLERY</Text>
                        </TouchableOpacity>

                        <TouchableOpacity onPress={() => this._removeImage(this.state.placeOfModal)} style={[imageSelectionButton, { backgroundColor: '#f50025' }]}>
                            <Text style={{ color: '#fff' }}>REMOVE IMAGE</Text>
                        </TouchableOpacity>

                        <TouchableOpacity onPress={() => this.setimageSlectModalVisible(false)} style={[imageSelectionButton, { backgroundColor: '#ddd' }]}>
                            <Text style={{ color: '#fff' }}>CANCEL</Text>
                        </TouchableOpacity>
                    </View>
                </View>
            </Modal>
        );
    }

    render() {
        const { imageContainer,
                listContainer,
                formItemContainer,
                formInputs,
                buttonContainer,
                buttonText,
                modalTitle,
                modalContainer,
                modalItem,
                headerContainer
             } = styles;

             const _renderTextOfSlection = (text, defaultText) => {
                if (text === '' || typeof text === 'undefined') {
                    return (<Text style={{ color: '#C7C7CD', fontSize: 17 }}>{defaultText}</Text>);
                }

                return (<Text>{text}</Text>);
             };

             const _renderStoreList = this.state.storeData.map((data, index) => {
                 return (
                    <TouchableOpacity key={data.id} style={modalItem} onPress={() => { this.setStoreModalVisible(); }}>
                        <Text>{data.name}</Text>
                    </TouchableOpacity>
                 );
             });

        const { image1, image2, image3, image4 } = this.state;
        
        return (
            <View behavior='padding' style={{ flex: 1 }}>
                <View style={headerContainer}>                
                    <Header
                        leftComponent={this.renderBackButton()}
                        centerComponent={<HeaderText title="Ad Post" />}
                        outerContainerStyles={{ backgroundColor: '#f50025', height: 82 }}
                        innerContainerStyles={{ justifyContent: 'flex-start' }}
                    />
                </View>
                <View style={[imageContainer]}>
                    {this._renderImage(image1, 1)}
                    {this._renderImage(image2, 2)}
                    {this._renderImage(image3, 3)}
                    {this._renderImage(image4, 4)}
                </View>
                <ScrollView style={[listContainer, { position: 'relative' }]}>

                    <View style={formItemContainer}>
                        <TouchableOpacity style={formInputs} onPress={() => { this.setLocationModalVisible(); }}>
                        {_renderTextOfSlection(this.props.store.location.city_name, 'Location')}
                        </TouchableOpacity>
                    </View>

                    <View style={formItemContainer}>
                        <TouchableOpacity style={formInputs} onPress={() => { this.setCategoryModalVisible(); }}>
                            {_renderTextOfSlection(this.props.store.category.child_name, 'Category')}
                        </TouchableOpacity>
                    </View>

                    <View style={formItemContainer}>
                        <View style={formInputs}>
                            <TextInput
                                placeholder="Item Name"
                                underlineColorAndroid='transparent'
                                value={this.state.itemName}
                                onChangeText={this._itemNameChange.bind(this)}  
                            />
                        </View>
                    </View>

                    <View style={formItemContainer}>
                        <View style={formInputs}>
                            <TextInput
                                placeholder="Price MVR"
                                keyboardType="numeric"
                                underlineColorAndroid='transparent'
                                value={this.state.price}
                                onChangeText={this._priceChange.bind(this)} 
                            />
                        </View>
                    </View>

                    <View style={formItemContainer}>
                        <View style={formInputs}>
                            <TextInput
                                placeholder="Contact No"
                                keyboardType="phone-pad"
                                underlineColorAndroid='transparent'
                                value={this.state.contactNo}
                                onChangeText={this._contactNoChange.bind(this)} 
                            />
                        </View>
                    </View>

                    <View style={formItemContainer}>
                        <TouchableOpacity style={formInputs} onPress={() => { this.setStoreModalVisible(); }}>
                        {_renderTextOfSlection(this.props.store.store.st_name, 'Store')}
                        </TouchableOpacity>
                    </View>

                    <View style={{ flexDirection: 'row' }}>
                        <View style={formInputs}>
                            <TextInput
                                placeholder="Description"
                                underlineColorAndroid='transparent'
                                multiline
                                numberOfLines={8}
                                blurOnSubmit={false}
                                editable
                                value={this.state.description}
                                onChangeText={this._descriptionChange.bind(this)} 
                            />
                        </View>
                    </View>

                </ScrollView>

                <View>
                    <LocationSelector showModal={this.props.store.showLocationModal} />
                    <CategorySelector showModal={this.props.store.showCategoryModal} />
                    <StoreSelector showModal={this.props.store.showStoreModal} />
                </View>


                <Modal
                    animationType="slide"
                    transparent={false}
                    visible={this.state.modalStoreVisible}
                    onRequestClose={() => { this.setStoreModalVisible(); }}
                >
                    <Text style={modalTitle}>Select Store Type</Text>
                    <ScrollView style={modalContainer}>
                        {_renderStoreList}
                    </ScrollView>
                </Modal>

                {this.renderImageSelectionModal()}

                {!this.state.isLoading && 
                <TouchableOpacity style={buttonContainer} onPress={() => this._createItem()}>
                    <Text style={buttonText}>POST</Text>
                </TouchableOpacity>}

                {this.state.isLoading && 
                <TouchableOpacity style={buttonContainer}>
                    <Spinner
                        size="large"
                        color="white"
                    />
                </TouchableOpacity>}
                
            </View>
        );
    }
}

const styles = {
    headerContainer: {
        marginBottom: 80
    },
    imageContainer: {
        height: 100,
        flexDirection: 'row'
    },
    listContainer: {
        flex: 1,
        backgroundColor: '#fff',
        paddingRight: 10,
        paddingLeft: 10
    },
    companyDetailsContainer: {
        padding: 10,
        paddingTop: 5,
        paddingBottom: 5,
    },
    itemImage: {
        flex: 1,
        margin: 2,
        width: '100%'
    },
    formContainer: {
        paddingRight: 10
    },
    formItemContainer: {
        flexDirection: 'row',
        height: 50,
        justifyContent: 'center',
    },
    formInputs: {
        flex: 1,
        justifyContent: 'center',
        borderBottomWidth: 0.5,
        borderColor: '#ddd',
    },
    formInputText: {
        fontSize: 10,
        fontWeight: '100',
        color: '#3e3e3e'
    },
    buttonContainer: {
        backgroundColor: '#f50025',
        height: 60,
        alignItems: 'center',
        justifyContent: 'center'
    },
    buttonText: {
        color: '#fff'
    },
    modalTitle: {
        backgroundColor: '#f50025',
        color: '#fff',
        padding: 5
    },
    modalContainer: {
        flex: 1,
        padding: 10
    },
    modalItem: {
        padding: 5,
        borderBottomWidth: 0.5,
        borderColor: '#c7c7cd'
    },
    imageSlectModal: {
        backgroundColor: '#fff',
        padding: 10,
        borderRadius: 5
    },
    imageSelectionButton: {
        backgroundColor: '#3ec2d7',
        marginBottom: 5,
        marginTop: 5,
        marginLeft: 10,
        marginRight: 10,
        padding: 15,
        justifyContent: 'center',
        alignItems: 'center'
    },
    categoryItemContainer: {
        borderBottomWidth: 0.5,
        borderColor: '#ddd',
        paddingTop: 5,
        paddingBottom: 5
    }
};


const mapStateToProps = (state) => ({
    nav: state.nav,
    settings: state.settings,
    store: state.store
});

const mapDispatchToProps = (dispatch) => ({ ...bindActionCreators(ActionCreators, dispatch), dispatch });

export default connect(mapStateToProps, mapDispatchToProps)(StroeScreen);
