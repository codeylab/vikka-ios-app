import React, { PureComponent } from 'react';
import { 
    View, 
    FlatList,
} from 'react-native';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import axios from 'axios';
// import { NavigationActions } from 'react-navigation';
// import ActionButton from 'react-native-action-button';

import * as ActionCreators from '../actions';

import ListStore from '../components/listStore';
import Spinner from '../components/spinner';

class StoreTabScreen extends PureComponent {
    constructor(props) {
        super(props);

        this.state = {
            isLoading: true,
            data: null,
        };
    }

    componentDidMount() {
        if (!this.state.data) {
            this._getData()
                .then((data) => {
                    this.setState({ data: data.store, isLoading: false });
                })
                .catch(err => { throw err; });
        }
    }

    async _getData() {
        const res = await axios('http://vikka.mv/core/getStoreList.php');
        return res.data;
    }

    _setScroll(status) {
        this.props.setScroll(status);            
    }

    renderContent() {
        if (!this.state.isLoading) {
            return (
                <FlatList
                    onTouchStart={this._setScroll.bind(this, false)}
                    onMomentumScrollEnd={this._setScroll.bind(this, true)}
                    onScrollEndDrag={this._setScroll.bind(this, true)}
                    showsVerticalScrollIndicator={false}
                    style={{ marginBottom: 5 }}
                    data={this.state.data}
                    keyExtractor={({ st_id }) => st_id}
                    renderItem={({ item, index }) => (<ListStore item={item} key={index} />)}
                    numColumns={1}
                    initialNumToRender={10}
                />
            );
        }
        return (
            <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                <Spinner
                    size="large"
                    color="red"
                />
            </View>
        );
    }

    render() {
        return (
            <View style={styles.container}>
                {this.renderContent()}
            </View>
        );
    }
}

const styles = {
   container: {
       flex: 1,
   }
};


const mapStateToProps = (state) => ({
    nav: state.nav,
    tab: state.tab,
    settings: state.settings
});

const mapDispatchToProps = (dispatch) => ({ ...bindActionCreators(ActionCreators, dispatch), dispatch });

export default connect(mapStateToProps, mapDispatchToProps)(StoreTabScreen);
