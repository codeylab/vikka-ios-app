import React, { Component } from 'react';
import { 
    View, 
    Text,
    StatusBar,
    TouchableOpacity,
    KeyboardAvoidingView,
    AsyncStorage,
} from 'react-native';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { NavigationActions } from 'react-navigation';
import base64 from 'base-64';
import axios from 'axios';

import * as ActionCreators from '../actions';

import LoginInputText from '../components/loginInputText';
import LoginButton from '../components/loginButton';

class RegisterScreen extends Component {
    constructor(props) {
        super(props);

        this.state = {
            email: '',
            name: '',
            password: '',
            mobile: '',
            data: [],
            error: false
        };
    }

    _navigateBackToLogin() {
        this.props.dispatch(NavigationActions.navigate({ routeName: 'Login' }));
    }

    
    async _registerUser() {
        const { email, password, name, mobile } = this.state;

        try {
            const encodedPassword = base64.encode(password);
            const url = `http://vikka.mv/core/user_reg.php?emid=${email}&uname=${name}&pws=${encodedPassword}&mob=${mobile}`;
            const userUrl = `http://vikka.mv/core/user_profile.php?user_email=${email}`;            
            const { data } = await axios.get(url);
            if (data.query_status[0].q_status === '1') {
                const userData = await axios.get(userUrl);

                await AsyncStorage.setItem('logedIn', JSON.stringify(true));

                if (userData.data.user_profile !== null) {
                    await AsyncStorage.setItem('user', JSON.stringify(userData.data.user_profile));                    
                }

                this.props.dispatch(NavigationActions.navigate({ routeName: 'Main' }));                  
            }
        } catch (e) {
            this.error = true;
        }
    }

    _emailChange(text) {
        this.setState({
            email: text
        });
    }

    _nameChange(text) {
        this.setState({
            name: text
        });
    }

    _passwordChange(text) {
        this.setState({
            password: text
        });
    }

    _mobileChange(text) {
        this.setState({
            mobile: text
        });
    }

    render() {
        const { mainContainer, 
            container, 
            welcomeTextContainer, 
            welcomeText, 
            loginTextContainer, 
            loginText, 
            singUpTextContainer
        } = styles;

        return (
            <KeyboardAvoidingView 
                behavior='padding'
				style={mainContainer}
            >
                <StatusBar hidden />

                <View style={container}>
                    <View style={welcomeTextContainer}>
                        <Text style={welcomeText}>Register</Text>
                    </View>

                    <LoginInputText 
                        autoCapitalize={'none'}
                        returnKeyType={'done'}
                        placeholder="Email Address"
                        autoCorrect={false}
                        topRound 
                        value={this.state.email}
                        onChangeText={this._emailChange.bind(this)}                        
                    />

                    <LoginInputText 
                        autoCapitalize={'none'}
                        returnKeyType={'done'}
                        placeholder="Name"
                        autoCorrect={false} 
                        value={this.state.name}
                        onChangeText={this._nameChange.bind(this)}
                    />

                    <LoginInputText 
                        autoCapitalize={'none'}
                        returnKeyType={'done'}
                        placeholder="Password"
                        autoCorrect={false}
                        secureTextEntry
                        value={this.state.password}
                        onChangeText={this._passwordChange.bind(this)}                        
                    />

                    <LoginInputText 
                        autoCapitalize={'none'}
                        returnKeyType={'done'}
                        placeholder="Mobile"
                        autoCorrect={false}
                        bottomRound 
                        value={this.state.mobile}
                        onChangeText={this._mobileChange.bind(this)}                                                
                    />

                    <LoginButton onPressBtn={() => this._registerUser()} buttonText="REGISTER" />

                </View>

                    <View style={[loginTextContainer, singUpTextContainer]}>
                        <TouchableOpacity onPress={() => this._navigateBackToLogin()}>
                            <Text style={loginText}>Have account? Sing In</Text>
                        </TouchableOpacity>
                    </View>
            </KeyboardAvoidingView>
        );
    }
}

const styles = {
    mainContainer: {
        flex: 1,
        backgroundColor: '#820d2a'
    },
    container: {
        flex: 1,
        justifyContent: 'center',
    },
    welcomeTextContainer: {
        alignItems: 'center',
        paddingBottom: 40
    },
    welcomeText: {
        color: 'white',
        fontSize: 32,
        fontWeight: 'bold'
    },
    loginTextContainer: {
        alignItems: 'center',
        marginTop: 10
    },
    loginText: {
        color: 'white',
        fontSize: 16
    },
    singUpTextContainer: {
        marginBottom: 40
    }
};


const mapStateToProps = (state) => ({
    nav: state.nav,
    settings: state.settings
});

const mapDispatchToProps = (dispatch) => ({ ...bindActionCreators(ActionCreators, dispatch), dispatch });

export default connect(mapStateToProps, mapDispatchToProps)(RegisterScreen);
