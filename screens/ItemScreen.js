import React, { Component } from 'react';
import { 
    View, 
    Text,
    ScrollView,
    TouchableOpacity,
    Share
} from 'react-native';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { Header, Icon } from 'react-native-elements';
import axios from 'axios';

import * as ActionCreators from '../actions';

import HeaderText from '../components/headerText';
import DetailCard from '../components/detailCard';
import ImageSlider from '../components/imageSlider';


class ItemScreen extends Component {
    constructor(props) {
        super(props);

        this.state = {
            isLoading: true,
            sliderImages: [],
        };
    }

    componentDidMount() {
        if (this.state.sliderImages.length === 0) {
            this._getData()
                .then((data) => {
                    const imgs = [];
                    data.ser_gallery.map(img => {
                        // const u = `http://www.vikka.mv/images/adds/adds/${img.im_path}`;
                        imgs.push(img);
                    });
                    this.setState({ sliderImages: imgs, isLoading: false });
                })
                .catch(err => { throw err; });
        }
    }

    async _getData() {
        const { ls_ad_id } = this.props.navigation.state.params.data;
        const res = await axios(`http://vikka.mv/core/gallery_xml_adds.php?a_id=${ls_ad_id}`);
        return res.data;
    }

     _navigateBackToLogin() {
        this.props.navigation.goBack();
    }

    _shareIt() {
        const { ls_ad_id } = this.props.navigation.state.params.data;        
        Share.share({
            url: `http://www.vikka.mv/ads-details.php?a_id=${ls_ad_id}`
        });
    }
    
    renderBackButton() {
        return (
            <TouchableOpacity onPress={() => this._navigateBackToLogin()}>
                <Icon name='arrow-back' color="white" />               
            </TouchableOpacity>
        );
    }

    renderRightButton() {
        return (
            <TouchableOpacity onPress={() => this._shareIt()}>
                <Icon name='share' color="white" />               
            </TouchableOpacity>
        );
    }

    render() {
        const { 
            scroll,
            headerContainer,
            itemTitleContainer,
            itemTitleText,
            itemPriceText,
        } = styles;

        const { ls_ad_id,
                ct1_id,
                ct2_id,
                ct1_name,
                ct2_name,
                ls_location_id,
                ls_price,
                ls_contact,
                ls_heading,
                ls_description,
                ls_post_date,
                ls_store_id,
                ls_ad_status,
                ls_store_name,
                ls_favorite,
                ls_loc_address,
                ls_image 
            } = this.props.navigation.state.params.data;

        return (
            <View style={{ flex: 1 }}>
                <View style={headerContainer}>                
                    <Header
                        leftComponent={this.renderBackButton()}
                        centerComponent={<HeaderText marginLeft={0} title="Details" />}
                        rightComponent={this.renderRightButton()}                        
                        outerContainerStyles={{ backgroundColor: '#f50025', height: 82, width: '100%' }}
                    />
                </View>
                
                <ScrollView style={scroll} >
                    <ImageSlider
                        images={this.state.sliderImages}
                    />
                    
                    <View style={itemTitleContainer} elevation={1}>
                        <Text style={itemTitleText}>{ls_heading}</Text>
                        <Text style={itemPriceText}>MVR {ls_price}</Text>
                    </View>

                    <DetailCard
                        title="Description"
                        type="singleLine"
                        data={[ls_description]}
                    />

                    <DetailCard
                        title="Details"
                        type="multiLine"
                        data={[
                            { Location: ls_loc_address },
                            { Contact: ls_contact },
                            { 'Post Date': ls_post_date },
                            { Category: `${ct1_name}, ${ct2_name}` },
                            { Store: ls_store_name },
                        ]}
                    />

                    <DetailCard
                        title="Buyer Protection"
                        type="staticLine"
                    />                    
                </ScrollView>
            </View>
        );
    }
}

const styles = {
    scroll: {
        backgroundColor: '#e4e4e4'
    },
    headerContainer: {
        width: '100%',
        marginBottom: 80,
    },
    itemTitleContainer: {
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#fff',
    },
    itemTitleText: {
        paddingTop: 10,
        paddingBottom: 10,
        fontSize: 16
    },
    itemPriceText: {
        paddingBottom: 10,
        fontSize: 18
    },
    leftContainer: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'flex-start',
    },
    shareContainer: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'flex-end',
        alignItems: 'center',
    },
};


const mapStateToProps = (state) => ({
    nav: state.nav,
    settings: state.settings
});

const mapDispatchToProps = (dispatch) => ({ ...bindActionCreators(ActionCreators, dispatch), dispatch });

export default connect(mapStateToProps, mapDispatchToProps)(ItemScreen);
