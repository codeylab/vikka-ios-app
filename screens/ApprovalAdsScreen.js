import React, { Component } from 'react';
import { 
    Text, 
    View, 
    Image,
    Dimensions,
    AsyncStorage,
    TouchableOpacity,
    ScrollView
} from 'react-native';
import axios from 'axios';
import { NavigationActions } from 'react-navigation';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import Icon from '@expo/vector-icons/FontAwesome';
import NewestIcon from '@expo/vector-icons/SimpleLineIcons';

import * as ActionCreators from '../actions';

class ApprovalAdsScreen extends Component {
    constructor(props) {
        super(props);
        
        this.state = {
            data: []
        };
    }

    async componentDidMount() {
        this.fetchData();
    }

    async fetchData() {
        let user = await AsyncStorage.getItem('user');
        if (user !== null) {
            user = JSON.parse(user)[0];
            try {
                const url = `http://vikka.mv/core/app_list_store_view.php?em_id=${user.uEmail_h}`;
                const { data } = await axios.get(url);

                if (data.list_view != null) {
                    const result = [];

                    data.list_view.map(item => {
                        if (item.ls_ad_status == 1) {
                            result.push(item);
                        }
                    });
        
                    this.setState({
                        data: result
                    });
                } else {
                    this.setState({
                        data: []
                    });
                }
            } catch (e) {
                this.setState({
                    data: []
                });
            }            

        }
    }

    _onPressButton(item) {
        this.props.dispatch(NavigationActions.navigate({ 
            routeName: 'Item', 
            params: {
                data: item
            }
        }));
    }

    render() {
        const renderList = () => {
            const {
                itemContainer,
                itemImageContainer,
                itemDetailContainer,
                itemTitle,
                itemDescription,
                itemLocation,
                itemCategory,
                itemPrice,
                itemImage,
            } = styles;

            const listOfItems = this.state.data.map((item, index) => {
                return (
                    <TouchableOpacity key={index} onPress={() => this._onPressButton(item)}>
                        <View style={itemContainer}>
                            <View style={itemImageContainer}>
                                <Image
                                    style={[itemImage, { height: '100%' }]}
                                    source={{ uri: `http://www.vikka.mv/images/adds/adds/${item.ls_image}` }}
                                />
                            </View>
                            <View style={itemDetailContainer}>
                                <Text style={itemTitle}>{item.ls_heading}</Text>
                                <Text style={itemDescription}>{item.ls_description}</Text>
                                <View style={{ flexDirection: 'row', marginTop: 10, marginBottom: 10 }}>
                                    <Text style={itemLocation}>{item.ls_loc_address}, </Text>
                                    <Text style={itemCategory}>{item.ct1_name}</Text>
                                </View>
                                <View style={{ flexDirection: 'row' }}>
                                    <Text style={[itemPrice, { alignSelf: 'flex-start' }]}>MVR {item.ls_price} </Text>
                                    <View style={{ flex: 1, flexDirection: 'row', alignSelf: 'flex-end', justifyContent: 'flex-end', marginTop: -10 }}>
                                        <View style={{ paddingRight: 5 }}>
                                            <Icon name='trash-o' color="gray" size={25} />
                                        </View>
                                        <View style={{ paddingTop: 3 }}>
                                            <NewestIcon name='pencil' color="gray" size={20} />
                                        </View>
                                    </View>
                                </View>
                            </View>
                        </View>
                    </TouchableOpacity>
                );
            });

            return listOfItems;
        };

        const renderEmpty = () => {
            return (
                <View>
                    <Text style={{ color: 'gray', fontSize: 15, fontWeight: '400', textAlign: 'center', marginTop: 10 }}>Post Ads</Text>
                    <View
                        style={{
                            marginTop: 10,
                            borderBottomColor: 'gray',
                            borderBottomWidth: 1,
                        }}
                    />
                    
                    <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center', marginTop: 5, paddingBottom: 10 }}>
                        <Image
                            source={{ uri: 'http://www.pvhc.net/img21/qeuujmybataqjfecunxl.png' }}
                            style={{ width: Dimensions.get('window').height / 2.9, height: Dimensions.get('window').height / 2.9 }}
                        />    
                    </View>  

                    <Text style={{ color: 'black', fontSize: 15, fontWeight: 'bold', textAlign: 'center', marginTop: 10 }}>You have not post any Ads</Text> 
                    <Text style={{ color: 'gray', fontSize: 13, textAlign: 'center', marginTop: 10 }}>We have enable you to post ads. Firt five ads are free.Would you like to post ads.</Text>                 
                </View>
            );
        };

        const showList = () => {
            if (this.state.data != null && this.state.data.length > 0) {
                return renderList();
            } 

            return renderEmpty();
        };

        return (
            <ScrollView style={[styles.scrollList, { width: Dimensions.get('window').width }]}>
                {showList()}
            </ScrollView>
        );
    }
}

const styles = {
    scroll: {
        backgroundColor: '#e4e4e4'
    },
    headerContainer: {
        marginBottom: 80
    },
    itemTitleContainer: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#fff',
    },
    itemTitleText: {
        paddingTop: 10,
        paddingBottom: 10,
        fontSize: 16
    },
    itemPriceText: {
        paddingBottom: 10,
        fontSize: 18
    },
    buttonContainer: {
        backgroundColor: '#f50025',
        height: 60,
        alignItems: 'center',
        justifyContent: 'center'
    },
    buttonText: {
        color: '#fff'
    },
    scrollList: {
        paddingLeft: 10,
        paddingRight: 10,
    },
    imageContainer: {
        flex: 1,
        backgroundColor: '#fff'
    },
    listContainer: {
        flex: 2,
        backgroundColor: '#fff'
    },
    companyDetailsContainer: {
        padding: 10,
        paddingTop: 5,
        paddingBottom: 5,
    },
    coverImage: {
        flex: 2
    },
    profileImage: {
        flex: 1,
        borderWidth: 2,
        borderRadius: 2,
        borderColor: '#fff',
        shadowColor: '#000',
        shadowOpacity: 0.75,
        shadowOffset: { height: 0, width: 0 },
        position: 'relative',
        marginLeft: 10
    },
    companyName: {
        position: 'relative',
        fontWeight: 'bold',
    },
    companyAddress: {
        fontSize: 12,
        color: '#808080'
    },
    companyDrescription: {
        fontSize: 12,
        marginTop: 5
    },
    companyWebsite: {
        fontSize: 12,
        color: '#06a9d1'
    },
    companyContact: {
        fontSize: 12   
    },
    openHours: {
        fontSize: 12        
    },
    storeType: {
        marginTop: 10,
        fontWeight: 'bold',
        color: '#808080'
    },
    itemContainer: {
        // flex: 1,
        flexDirection: 'row',
        borderColor: '#ddd',
        borderTopWidth: 0.5,
        borderBottomWidth: 0.5,
        paddingTop: 10,
        paddingBottom: 10
    },
    itemImageContainer: {
        flex: 1,
        borderRadius: 2,
        borderWidth: 0.5,
        borderColor: '#000'
    },
    itemDetailContainer: {
        flex: 3,
        paddingLeft: 15  
    },
    itemDescription: {
        color: '#808080',
        fontSize: 10
    },
    itemLocation: {
        color: '#808080',
    },
    itemCategory: {
        color: '#808080',
    },
    itemImage: {
        flex: 1,
        borderWidth: 2,
        borderRadius: 2,
        borderColor: '#fff',
        shadowColor: 'red',
        shadowOpacity: 0.5,
        shadowOffset: { height: 2, width: 2 },
    }
};


const mapStateToProps = (state) => ({
    nav: state.nav,
    settings: state.settings
});

const mapDispatchToProps = (dispatch) => ({ ...bindActionCreators(ActionCreators, dispatch), dispatch });

export default connect(mapStateToProps, mapDispatchToProps)(ApprovalAdsScreen);
