import React, { Component } from 'react';
import { 
    FlatList, 
    View, 
    Text, 
    ImageBackground, 
    TouchableWithoutFeedback, 
    Dimensions,
    ScrollView,
} from 'react-native';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import * as ActionCreators from '../actions';

import AdContainer from '../components/adContainer';

class HomeScreen extends Component {
    constructor(props) {
        super(props);

        this.state = {
            data: [
                {
                    id: 14,
                    name: 'Electronics',
                    image: 'https://image.ibb.co/jhczoR/electronic.png'
                },
                {
                    id: 15,
                    name: 'Automobile',
                    image: 'https://image.ibb.co/jSOpoR/vehicals.png'
                },
                {
                    id: 16,
                    name: 'Property',
                    image: 'https://image.ibb.co/dhR3F6/real_estate.png'
                },
                {
                    id: 17,
                    name: 'Home & Garden',
                    image: 'https://image.ibb.co/mJsSa6/home.jpg'
                },
                {
                    id: 18,
                    name: 'Fashion',
                    image: 'https://image.ibb.co/bRPb2m/beauty.png'
                },
                {
                    id: 19,
                    name: 'Hobby, Sport & Kids',
                    image: 'https://image.ibb.co/nfY5TR/sport.png'
                },
                {
                    id: 20,
                    name: 'Business',
                    image: 'https://image.ibb.co/mm0PNm/business.png'
                },
                {
                    id: 21,
                    name: 'Education',
                    image: 'https://image.ibb.co/nDPyF6/education.png'
                },
                {
                    id: 22,
                    name: 'Animals',
                    image: 'https://image.ibb.co/ezgaTR/pets.png'
                },
                {
                    id: 23,
                    name: 'Food & Agriculture',
                    image: 'https://image.ibb.co/mLBLv6/food.jpg'
                },
                {
                    id: 24,
                    name: 'Other',
                    image: 'https://image.ibb.co/hLWENm/other.jpg'
                },
                {
                    id: 25,
                    name: 'ALL Adds',
                    image: 'https://image.ibb.co/kPnca6/all_ads.png'
                },
                {
                    id: 26,
                    name: 'Jobs',
                    image: 'https://image.ibb.co/c7nAv6/job.png'
                },
               
            ]
        };
    }

    _nav(item) {
        const { navigate } = this.props.navigation;
        navigate('Search', {
            item
        });
    }

    _setScroll(status) {
        this.props.setScroll(status);            
    }

    _renderItem(item, index) {
        const marginRight = (index % 2 === 0) ? 0 : 2;

        return (
                <View key={index} style={[styles.container, { marginRight }]}>
                    <TouchableWithoutFeedback onPress={() => this._nav(item)}>
                        <ImageBackground 
                            style={[styles.image]}
                            source={{ uri: item.image }}
                            resizeMode={'stretch'}
                        >
                            <Text style={styles.text}>{item.name}</Text>
                        </ImageBackground>
                    </TouchableWithoutFeedback>
                </View>      
        );
    }

    render() {
        return (
            <ScrollView>

                <FlatList
                    onTouchStart={this._setScroll.bind(this, false)}
                    onMomentumScrollEnd={this._setScroll.bind(this, true)}
                    onScrollEndDrag={this._setScroll.bind(this, true)}
                    showsVerticalScrollIndicator={false}
                    style={{ marginBottom: 5 }}
                    data={this.state.data}
                    keyExtractor={({ id }) => id}
                    renderItem={({ item, index }) => this._renderItem(item, index)}
                    numColumns={2}
                    initialNumToRender={15}
                />

                <AdContainer image="http://4.bp.blogspot.com/-97L6x07YwuI/WGTVa2yepFI/AAAAAAAAABc/AXopRuf_9R4Fo-c-FXcwjR85XA-IzD9ogCK4B/s1600/20071107your_ad_here.png" />
                <AdContainer image="http://4.bp.blogspot.com/-97L6x07YwuI/WGTVa2yepFI/AAAAAAAAABc/AXopRuf_9R4Fo-c-FXcwjR85XA-IzD9ogCK4B/s1600/20071107your_ad_here.png" />

            </ScrollView>
        );
    }
}

const styles = {
    container: {
        flex: 1, 
        width: (Dimensions.get('window').width - 6) / 2, 
        height: (Dimensions.get('window').width) / 2.5, 
        margin: 2,
        marginBottom: 0
    },
    image: {
        flexGrow: 1,
        height: null,
    },
    text: {
        backgroundColor: 'transparent',
        position: 'absolute',
        bottom: 5,
        left: 5,
        color: '#FFF'
    }
};


const mapStateToProps = (state) => ({
    nav: state.nav,
    settings: state.settings
});

const mapDispatchToProps = (dispatch) => ({ ...bindActionCreators(ActionCreators, dispatch), dispatch });

export default connect(mapStateToProps, mapDispatchToProps)(HomeScreen);
