import React, { PureComponent } from 'react';
import {  
    View,
    Text, 
    Image,
    ImageBackground,
    TouchableOpacity,
    FlatList
} from 'react-native';
import { Icon } from 'react-native-elements';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { NavigationActions } from 'react-navigation';
import axios from 'axios';

import * as ActionCreators from '../actions';

import ItemListOfStore from '../components/itemListOfStore';
import Spinner from '../components/spinner';

class StroeScreen extends PureComponent {
    constructor(props) {
        super(props);

        this.state = {
            view2LayoutProps: {
                width: 0,
                height: 0,
                itemHeight: 0,
                itemWidth: 0
            },
            data: null,
            storeData: null,
            isLoading: true,
            isStoreLoading: true,
        };
    }

    componentDidMount() {
        if (!this.state.data) {
            this._getData()
                .then((data) => {
                    this.setState({ data: data.list_view, isLoading: false });
                })
                .catch(err => { throw err; });
        }

        if (!this.state.storeData) {
            this._getStoreData()
                .then((data) => {
                    this.setState({ storeData: data.store[0], isStoreLoading: false });
                })
                .catch(err => { throw err; });
        }
    }

    onLayout(event) {
        const { height, width } = event.nativeEvent.layout;

        this.setState({ height, width });
    }

    onLayoutItem(event) {
        const { height, width } = event.nativeEvent.layout;

        this.setState({ itemHeight: height, itemWidth: width });
    }

    async _getData() {
        const { state } = this.props.navigation;
        const res = await axios(`http://vikka.mv/core/app_list_store_view.php?st_id=${state.params.st_id}`);
        return res.data;
    }

    async _getStoreData() {
        const { state } = this.props.navigation;
        const res = await axios(`http://vikka.mv/core/getStore.php?st_id=${state.params.st_id}`);
        return res.data;
    }

    _navigateBackToStoreList() {
        this.props.navigation.goBack();
    }

    _onPressButton() {
        this.props.dispatch(NavigationActions.navigate({ routeName: 'Item' }));
    }

    _setScroll(status) {
        this.props.setScroll(status);            
    }


    renderBackButton() {
        return (
            <TouchableOpacity style={{ height: 100, width: 50, justifyContent: 'space-around' }} onPress={() => this._navigateBackToStoreList()}>
                <Icon name='arrow-back' color="white" />               
            </TouchableOpacity>
        );
    }

    renderContent() {
        const { imageContainer,
                listContainer,
                coverImage,
                profileImage,
                companyDetailsContainer,
                companyName,
                companyAddress,
                companyDrescription,
                companyWebsite,
                companyContact,
                openHours,
                storeType,
                scrollList
             } = styles;

        if (!this.state.isLoading && !this.state.isStoreLoading) {
            const { 
                store_back_image,
                store_profile_image,
                store_name,
                store_address,
                store_description,
                store_website,
                store_contact,
                store_open_hrs,
                store_type,
            } = this.state.storeData;

            return (
                <View style={{ flex: 1 }}>
                    <View style={[imageContainer, { marginBottom: '-11%' }]}>
                        <ImageBackground
                            style={coverImage}
                            source={{ uri: `http://vikka.mv/images/store/${store_back_image}` }}
                        >
                            {this.renderBackButton()}
                        </ImageBackground>
                        <Image
                            style={[profileImage, { width: '19%', top: '-17%' }]}
                            source={{ uri: `http://vikka.mv/images/store/${store_profile_image}` }}
                        />
                    </View>
                    <View style={[listContainer, { position: 'relative' }]}>
                        <View style={companyDetailsContainer}>
                            <Text style={companyName}>{store_name}</Text>
                            <Text style={companyAddress}>{store_address}</Text>
                            <Text style={companyDrescription}>{store_description}</Text>
                            <TouchableOpacity>
                                <Text style={companyWebsite}>{store_website}</Text>
                            </TouchableOpacity>
                            <Text style={companyContact}>{store_contact}</Text>
                            <Text style={openHours}>{store_open_hrs}</Text>                    
                            <Text style={storeType}>{store_type}</Text>
                        </View>

                        <FlatList
                            onTouchStart={this._setScroll.bind(this, false)}
                            onMomentumScrollEnd={this._setScroll.bind(this, true)}
                            onScrollEndDrag={this._setScroll.bind(this, true)}
                            showsVerticalScrollIndicator={false}
                            style={scrollList}
                            data={this.state.data}
                            keyExtractor={({ ls_ad_id }) => ls_ad_id}
                            renderItem={({ item, index }) => (<ItemListOfStore item={item} key={index} />)}
                            numColumns={1}
                            initialNumToRender={10}
                        />

                    </View>
                </View>
            );
        }

        return (
            <View style={{ flex: 1, justifyContent: 'center' }}>
                <Spinner
                    size="large"
                    color="red"
                />
            </View>
        );
    }

    render() {
        return (
            <View style={{ flex: 1, backgroundColor: 'white' }}>
                {this.renderContent()}
            </View>
        );
    }
}

const styles = {
    imageContainer: {
        flex: 1,
        backgroundColor: '#fff'
    },
    listContainer: {
        flex: 2,
        backgroundColor: '#fff'
    },
    companyDetailsContainer: {
        padding: 10,
        paddingTop: 5,
        paddingBottom: 5,
    },
    coverImage: {
        flex: 2
    },
    profileImage: {
        flex: 1,
        borderWidth: 2,
        borderRadius: 2,
        borderColor: '#fff',
        shadowColor: '#000',
        shadowOpacity: 0.75,
        shadowOffset: { height: 0, width: 0 },
        position: 'relative',
        marginLeft: 10
    },
    companyName: {
        position: 'relative',
        fontWeight: 'bold',
    },
    companyAddress: {
        fontSize: 12,
        color: '#808080'
    },
    companyDrescription: {
        fontSize: 12,
        marginTop: 5
    },
    companyWebsite: {
        fontSize: 12,
        color: '#06a9d1'
    },
    companyContact: {
        fontSize: 12   
    },
    openHours: {
        fontSize: 12        
    },
    storeType: {
        marginTop: 10,
        fontWeight: 'bold',
        color: '#808080'
    },
    scrollList: {
        paddingLeft: 10,
        paddingRight: 10,
        marginBottom: 5
    }
};


const mapStateToProps = (state) => ({
    nav: state.nav,
    settings: state.settings
});

const mapDispatchToProps = (dispatch) => ({ ...bindActionCreators(ActionCreators, dispatch), dispatch });

export default connect(mapStateToProps, mapDispatchToProps)(StroeScreen);
