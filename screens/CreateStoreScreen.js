import React, { Component } from 'react';
import {  
    View,
    Text, 
    Image,
    ImageBackground,
    TouchableOpacity,
    ScrollView,
    TextInput,
    Modal,
    Dimensions,
    KeyboardAvoidingView,
    AsyncStorage,
    Alert
} from 'react-native';
import { Icon } from 'react-native-elements';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { ImagePicker } from 'expo';
import axios from 'axios';
import { NavigationActions } from 'react-navigation';

import LocationSelector from '../components/locationSelector';

import * as ActionCreators from '../actions';

class StroeScreen extends Component {
    constructor(props) {
        super(props);

        this.state = {
            view2LayoutProps: {
                width: 0,
                height: 0
            },
            companyName: '',
            address: '',
            description: '',
            website: '',
            contactNo: '',
            openingHours: '',
            location: 16,
            storeType: 'Store Type',
            coverImage: null,
            profileImage: null,
            placeOfModal: null,
            imageSelectModalVisible: false,
            modalStoreVisible: false,
            modalLocationVisible: false,
            locationName: 'Location',
            locationValue: 0,

            locationData: [
                {
                    id: 1,
                    name: 'Male'
                },
                {
                    id: 2,
                    name: 'Hul Hul Male'
                },
                {
                    id: 3,
                    name: 'Viliginilis'
                }
            ],

            storeData: [
                {
                    id: 1,
                    name: 'Retailer'
                },
                {
                    id: 2,
                    name: 'Whole Seller'
                },
                {
                    id: 3,
                    name: 'Other'
                },
            ]
        };
    }

    onLayout(event) {
        const { height, width } = event.nativeEvent.layout;

        this.setState({ height, width });
    }

    setLocationModalVisible() {
        this.props.showLocationModal(true);
    }

    setStoreModalVisible(name, visible) {
        this.setState({
            modalStoreVisible: visible,
            storeType: name
        });
    }

    setImageSlectModalVisible(visible, place) {
        this.setState({ imageSelectModalVisible: visible, placeOfModal: place });
    }

    async _createStore() {
        const { storeType, companyName, address, description, website, contactNo, openingHours, profileImage, coverImage } = this.state;
        const { dispatch } = this.props;
        const { location } = this.props.store;

        let isLogedIn = await AsyncStorage.getItem('logedIn');
        let user = await AsyncStorage.getItem('user');

        isLogedIn = JSON.parse(isLogedIn);
        user = JSON.parse(user)[0];  

        if (isLogedIn && companyName !== '' && address !== '' && description !== '' && contactNo !== '') {
            const url = `http://vikka.mv/core/create_store.php?st_name=${companyName}&st_add=${address}&st_desc=${description}&st_website=${website}&st_contact=${contactNo}&st_open_hrs=${openingHours}&st_location_id=${location.city_id}&st_type=${storeType}&st_user_email=${user.uEmail_h}&st_sql_stat=i`;

            try {
                const { data } = await axios.get(url);

                if (data.query_status[0].q_status === '1') {
                    const stId = data.query_status[0].st_id;

                    let profileImageName = profileImage.split('/').pop();
                    profileImageName = `${new Date().getTime()}.${profileImageName.split('.').pop()}`;

                    const matchProfile = /\.(\w+)$/.exec(profileImageName);
                    const profileImageType = matchProfile ? `image/${matchProfile[1]}` : 'image';

                    const profileFormData = new FormData();
                    profileFormData.append('filUpload', { uri: profileImage, name: profileImageName, type: profileImageType });

                    const profileImageUploadUrl = `http://vikka.mv/core/store_img_upload.php?st_id=${stId}&email_id=${user.uEmail_h}&img_path=${profileImageName}&im_count=1&st_sql_stat=u`;
                    
                    const coverImageName = coverImage.split('/').pop();
                    const matchCover = /\.(\w+)$/.exec(coverImageName);
                    const coverImageType = matchCover ? `image/${matchCover[1]}` : 'image';

                    const coverFormData = new FormData();
                    coverFormData.append('filUpload', { uri: coverImage, name: coverImageName, type: coverImageType });

                    const coverImageUploadUrl = `http://vikka.mv/core/store_img_upload.php?st_id=${stId}&email_id=${user.uEmail_h}&img_path=${coverImageName}&im_count=0&st_sql_stat=u`;                    

                    try {
                        await axios.post(coverImageUploadUrl, coverFormData, { headers: { 'Content-Type': 'multipart/form-data' } }); 
                        await axios.post(profileImageUploadUrl, profileFormData, { headers: { 'Content-Type': 'multipart/form-data' } });     
                        
                        dispatch(NavigationActions.navigate({ routeName: 'MyStore' }));
                    } catch (e) {
                    }            
                }
            } catch (e) {
            }
        }        
    }

    _pickImage = async (place) => {
        const result = await ImagePicker.launchImageLibraryAsync({
            allowsEditing: false,
            aspect: [4, 3],
        }); 
    
        if (!result.cancelled) {
            if (place === 1) {
                this.setState({ coverImage: result.uri });
            }
            if (place === 2) {
                this.setState({ profileImage: result.uri });
            }
        }

        this.setImageSlectModalVisible(false, null);
    };

    _takePhoto = async (place) => {  
        const { Permissions } = Expo;    
        const permissions = Permissions.CAMERA;
        const status = await Permissions.askAsync(permissions);

        if (status.status !== 'granted') {
            this.setimageSlectModalVisible(false, null);    
            Alert.alert('Permission Rejected', 'Sorry, We can\'t access the camera');
        } else {
            const result = await ImagePicker.launchCameraAsync({
                allowsEditing: false,
                aspect: [4, 3],
            });
    
            if (!result.cancelled) {
                if (place === 1) {
                    this.setState({ coverImage: result.uri });
                }
                if (place === 2) {
                    this.setState({ profileImage: result.uri });
                }
            }

            this.setimageSlectModalVisible(false, null);      
        }
    };

    _removeImage = async (place) => {
        if (place === 1) {
            this.setState({ coverImage: null });
        }
        if (place === 2) {
            this.setState({ profileImage: null });
        }
        
        this.setImageSlectModalVisible(false, null);        
    }

    _navigateBackToStoreList() {
        this.props.navigation.goBack();
    }

    _addressChange(text) {
        this.setState({
            address: text
        });
    }

    _companyNameChange(text) {
        this.setState({
            companyName: text
        });
    }

    _contactNoChange(text) {
        this.setState({
            contactNo: text
        });
    }

    _descriptionChange(text) {
        this.setState({
            description: text
        });
    }

    _openingHoursChange(text) {
        this.setState({
            openingHours: text
        });
    }

    _websiteChange(text) {
        this.setState({
            website: text
        });
    }

    renderBackButton() {
        return (
            <TouchableOpacity style={{ height: 100, width: 50, justifyContent: 'space-around' }} onPress={() => this._navigateBackToStoreList()}>
                <Icon name='arrow-back' color="white" />               
            </TouchableOpacity>
        );
    }

    renderImageSelectionModal() {
        const { imageSlectModal, imageSelectionButton } = styles;
        const WIDTH = Dimensions.get('window').width;

        return (
            <Modal
                animationType="fade"
                transparent
                visible={this.state.imageSelectModalVisible}
                onRequestClose={() => { this.setImageSlectModalVisible(false); }}
            >
                <View 
                    style={{
                        flex: 1,
                        justifyContent: 'center',
                        alignItems: 'center',
                        backgroundColor: 'rgba(0,0,0,0.5)'
                    }}
                >
                    <View style={[imageSlectModal, { width: WIDTH - 20 }]}>
                        <View style={{ padding: 5, alignItems: 'center' }}>
                            <Text>Choose a photo</Text>
                        </View>
                        <TouchableOpacity style={imageSelectionButton} onPress={() => this._takePhoto(this.state.placeOfModal)}>
                            <Text style={{ color: '#fff' }}>FROM CAMERA</Text>
                        </TouchableOpacity>

                        <TouchableOpacity style={imageSelectionButton} onPress={() => this._pickImage(this.state.placeOfModal)}>
                            <Text style={{ color: '#fff' }}>FROM GALLERY</Text>
                        </TouchableOpacity>

                        <TouchableOpacity onPress={() => this._removeImage(this.state.placeOfModal)} style={[imageSelectionButton, { backgroundColor: '#f50025' }]}>
                            <Text style={{ color: '#fff' }}>REMOVE IMAGE</Text>
                        </TouchableOpacity>
                        <TouchableOpacity onPress={() => this.setImageSlectModalVisible(false)} style={[imageSelectionButton, { backgroundColor: '#ddd' }]}>
                            <Text style={{ color: '#fff' }}>CANCEL</Text>
                        </TouchableOpacity>
                    </View>
                </View>
            </Modal>
        );
    }

    renderCoverImage(img) {
        if (img) {
            return (
                <TouchableOpacity style={{ flex: 2 }} onPress={() => { this.setImageSlectModalVisible(true, 1); }}>
                    <ImageBackground
                        style={styles.coverImage}
                        source={{ uri: img }}
                    >
                        {this.renderBackButton()}
                    </ImageBackground>
                </TouchableOpacity>
            );
        }
        return (
            <TouchableOpacity style={{ flex: 2 }} onPress={() => { this.setImageSlectModalVisible(true, 1); }}>
                <ImageBackground
                    style={styles.coverImage}
                    source={require('../assets/image_ad_post.png')}
                >
                    {this.renderBackButton()}
                </ImageBackground>
            </TouchableOpacity>
        );
    }

    renderProfileImage(img) {
        if (img) {
            return (
                <TouchableOpacity
                    onPress={() => { this.setImageSlectModalVisible(true, 2); }}
                    style={[styles.profileImage, { width: '30%', top: '-17%' }]}
                >
                    <Image
                        style={{ flex: 1 }}
                        source={{ uri: img }}
                    />
                </TouchableOpacity>
            );
        }
        return (
            <TouchableOpacity
                onPress={() => { this.setImageSlectModalVisible(true, 2); }}
                style={[styles.profileImage, { width: '30%', top: '-17%' }]}
            >
                <Image
                    style={{ flex: 1, width: '100%' }}
                    source={require('../assets/image_ad_post.png')}
                />
            </TouchableOpacity>
        );
    }

    render() {
        const { imageContainer,
                listContainer,
                formItemContainer,
                formIconContainer,
                formIcon,
                formInputs,
                buttonContainer,
                buttonText,
                modalTitle,
                modalContainer,
                modalItem
             } = styles;
        
        const { coverImage, profileImage } = this.state;

        const _renderTextOfSlection = (text) => {
        if (text === 'Store Type') {
            return (<Text style={{ color: '#C7C7CD' }}>{text}</Text>);
        } else if (text === 'Location' || typeof text === 'undefined') {
            return (<Text style={{ color: '#C7C7CD' }}>Location</Text>);
        }
            return (<Text>{text}</Text>);
        };

        const _renderStoreList = this.state.storeData.map((data, index) => {
            return (
            <TouchableOpacity key={data.id} style={modalItem} onPress={() => { this.setStoreModalVisible(data.name, false); }}>
                <Text>{data.name}</Text>
            </TouchableOpacity>
            );
        });


        return (
            <KeyboardAvoidingView behavior='padding' style={{ flex: 1 }}>
                <View style={[imageContainer, { marginBottom: '-11%' }]}>
                    {this.renderCoverImage(coverImage)}
                    {this.renderProfileImage(profileImage)}
                </View>
                <ScrollView style={[listContainer, { position: 'relative' }]}>

                    <View style={formItemContainer}>
                        <View style={formIconContainer}>
                            <Icon name='bank' type='material-community' iconStyle={formIcon} />
                        </View>
                        <View style={formInputs}>
                            <TextInput
                                placeholder="Company Name"
                                underlineColorAndroid='transparent'
                                value={this.state.companyName}
                                onChangeText={this._companyNameChange.bind(this)}  
                            />
                        </View>
                    </View>

                    <View style={formItemContainer}>
                        <View style={formIconContainer}>
                            <Icon name='link-variant' type='material-community' iconStyle={formIcon} />
                        </View>
                        <View style={formInputs}>
                            <TextInput
                                placeholder="Address"
                                underlineColorAndroid='transparent'
                                value={this.state.address}
                                onChangeText={this._addressChange.bind(this)}  
                            />
                        </View>
                    </View>

                    <View style={formItemContainer}>
                        <View style={formIconContainer}>
                            <Icon name='comment-processing-outline' type='material-community' iconStyle={formIcon} />
                        </View>
                        <View style={formInputs}>
                            <TextInput
                                placeholder="Description"
                                underlineColorAndroid='transparent'
                                value={this.state.description}
                                onChangeText={this._descriptionChange.bind(this)}  
                            />
                        </View>
                    </View>

                    <View style={formItemContainer}>
                        <View style={formIconContainer}>
                            <Icon name='web' type='material-community' iconStyle={formIcon} />
                        </View>
                        <View style={formInputs}>
                            <TextInput
                                placeholder="Website"
                                underlineColorAndroid='transparent'
                                value={this.state.website}
                                onChangeText={this._websiteChange.bind(this)}  
                            />
                        </View>
                    </View>

                    <View style={formItemContainer}>
                        <View style={formIconContainer}>
                            <Icon name='deskphone' type='material-community' iconStyle={formIcon} />
                        </View>
                        <View style={formInputs}>
                            <TextInput
                                placeholder="Contact No"
                                underlineColorAndroid='transparent'
                                value={this.state.contactNo}
                                onChangeText={this._contactNoChange.bind(this)}  
                            />
                        </View>
                    </View>

                    <View style={formItemContainer}>
                        <View style={formIconContainer}>
                            <Icon name='timer-sand' type='material-community' iconStyle={formIcon} />
                        </View>
                        <View style={formInputs}>
                            <TextInput
                                placeholder="Opening Hours"
                                underlineColorAndroid='transparent'
                                value={this.state.openingHours}
                                onChangeText={this._openingHoursChange.bind(this)}  
                            />
                        </View>
                    </View>

                    <View style={formItemContainer}>
                        <View style={formIconContainer}>
                            <Icon name='location' type='octicon' iconStyle={formIcon} />
                        </View>
                        <TouchableOpacity style={formInputs} onPress={() => { this.setLocationModalVisible(); }}>
                            {_renderTextOfSlection(this.props.store.location.city_name)}
                        </TouchableOpacity>
                    </View>

                    <View style={formItemContainer}>
                        <View style={formIconContainer}>
                            <Icon name='store' type='material-community' iconStyle={formIcon} />
                        </View>
                        <TouchableOpacity style={formInputs} onPress={() => { this.setStoreModalVisible(this.state.storeType, this.state.storeTypeValue, true); }}>
                            {_renderTextOfSlection(this.state.storeType)}
                        </TouchableOpacity>
                    </View>
                </ScrollView>

                <View>
                    <LocationSelector showModal={this.props.store.showLocationModal} />
                </View>
                

                <Modal
                    animationType="slide"
                    transparent={false}
                    visible={this.state.modalStoreVisible}
                    onRequestClose={() => { this.setStoreModalVisible(this.state.storeType, false); }}
                >
                    <Text style={modalTitle}>Select Store Type</Text>
                    <ScrollView style={modalContainer}>
                        {_renderStoreList}
                    </ScrollView>
                </Modal>

                {this.renderImageSelectionModal()}

                <TouchableOpacity style={buttonContainer} onPress={() => this._createStore()} >
                    <Text style={buttonText}>CREATE STORE</Text>
                </TouchableOpacity>
            </KeyboardAvoidingView>
        );
    }
}

const styles = {
    imageContainer: {
        flex: 1,
        backgroundColor: '#fff'
    },
    listContainer: {
        flex: 2,
        backgroundColor: '#fff'
    },
    companyDetailsContainer: {
        padding: 10,
        paddingTop: 5,
        paddingBottom: 5,
    },
    coverImage: {
        flex: 2,
        width: '100%'
    },
    profileImage: {
        flex: 1,
        borderWidth: 2,
        borderRadius: 2,
        borderColor: '#fff',
        shadowColor: '#000',
        shadowOpacity: 0.75,
        shadowOffset: { height: 0, width: 0 },
        position: 'relative',
        marginLeft: 10,
    },
    formContainer: {
        paddingRight: 10
    },
    formItemContainer: {
        flexDirection: 'row',
        height: 50,
        justifyContent: 'center',
    },
    formIconContainer: {
        justifyContent: 'center',
        paddingLeft: 20,
        paddingRight: 20
    },
    formIcon: {
        color: '#f50025'
    },
    formInputs: {
        flex: 1,
        justifyContent: 'center',
        borderBottomWidth: 0.5,
        borderColor: '#ddd',
    },
    formInputText: {
        fontSize: 10,
        fontWeight: '100',
        color: '#3e3e3e'
    },
    buttonContainer: {
        backgroundColor: '#f50025',
        height: 60,
        alignItems: 'center',
        justifyContent: 'center'
    },
    buttonText: {
        color: '#fff'
    },
    modalTitle: {
        backgroundColor: '#f50025',
        color: '#fff',
        padding: 5
    },
    modalContainer: {
        flex: 1,
        padding: 10
    },
    modalItem: {
        padding: 5,
        borderBottomWidth: 0.5,
        borderColor: '#c7c7cd'
    },
    imageSlectModal: {
        backgroundColor: '#fff',
        padding: 10,
        borderRadius: 5
    },
    imageSelectionButton: {
        backgroundColor: '#3ec2d7',
        marginBottom: 5,
        marginTop: 5,
        marginLeft: 10,
        marginRight: 10,
        padding: 15,
        justifyContent: 'center',
        alignItems: 'center'
    }
};


const mapStateToProps = (state) => ({
    nav: state.nav,
    settings: state.settings,
    store: state.store
});

const mapDispatchToProps = (dispatch) => ({ ...bindActionCreators(ActionCreators, dispatch), dispatch });

export default connect(mapStateToProps, mapDispatchToProps)(StroeScreen);
