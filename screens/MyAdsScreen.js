import React, { Component } from 'react';
import { 
    View, 
    Text,
    TouchableOpacity,
} from 'react-native';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { NavigationActions } from 'react-navigation';
import { Header, Icon as NewIcon } from 'react-native-elements';

import { AdsTabNavigator } from '../config/routes';
import * as ActionCreators from '../actions';

import HeaderText from '../components/headerText';

class MyAdsScreen extends Component {
     _navigateBackToLogin() {
        this.props.navigation.goBack();
    }

    _onPressButton() {
        this.props.dispatch(NavigationActions.navigate({ routeName: 'CreateItem' }));
    }

    renderBackButton() {
        return (
            <TouchableOpacity onPress={() => this._navigateBackToLogin()}>
                <NewIcon name='arrow-back' color="white" />               
            </TouchableOpacity>
        );
    }

    render() {
        const { 
            headerContainer,
            buttonContainer,
            buttonText
        } = styles;

        return (
            <View style={{ flex: 1 }}>
                <View style={headerContainer}>                
                    <Header
                        leftComponent={this.renderBackButton()}
                        centerComponent={<HeaderText title="My Ads" />}
                        outerContainerStyles={{ backgroundColor: '#f50025', height: 82 }}
                        innerContainerStyles={{ justifyContent: 'flex-start' }}
                    />
                </View>

                <AdsTabNavigator />

                <TouchableOpacity style={buttonContainer} onPress={() => this._onPressButton()}>
                    <Text style={buttonText}>Post Ads</Text>
                </TouchableOpacity>
            </View>
        );
    }
}

const styles = {
    scroll: {
        backgroundColor: '#e4e4e4'
    },
    headerContainer: {
        marginBottom: 80
    },
    itemTitleContainer: {
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#fff',
    },
    itemTitleText: {
        paddingTop: 10,
        paddingBottom: 10,
        fontSize: 16
    },
    itemPriceText: {
        paddingBottom: 10,
        fontSize: 18
    },
    buttonContainer: {
        backgroundColor: '#f50025',
        height: 60,
        alignItems: 'center',
        justifyContent: 'center'
    },
    buttonText: {
        color: '#fff'
    },
    scrollList: {
        paddingLeft: 10,
        paddingRight: 10
    },
    imageContainer: {
        flex: 1,
        backgroundColor: '#fff'
    },
    listContainer: {
        flex: 2,
        backgroundColor: '#fff'
    },
    companyDetailsContainer: {
        padding: 10,
        paddingTop: 5,
        paddingBottom: 5,
    },
    coverImage: {
        flex: 2
    },
    profileImage: {
        flex: 1,
        borderWidth: 2,
        borderRadius: 2,
        borderColor: '#fff',
        shadowColor: '#000',
        shadowOpacity: 0.75,
        shadowOffset: { height: 0, width: 0 },
        position: 'relative',
        marginLeft: 10
    },
    companyName: {
        position: 'relative',
        fontWeight: 'bold',
    },
    companyAddress: {
        fontSize: 12,
        color: '#808080'
    },
    companyDrescription: {
        fontSize: 12,
        marginTop: 5
    },
    companyWebsite: {
        fontSize: 12,
        color: '#06a9d1'
    },
    companyContact: {
        fontSize: 12   
    },
    openHours: {
        fontSize: 12        
    },
    storeType: {
        marginTop: 10,
        fontWeight: 'bold',
        color: '#808080'
    },
    scrollList: {
        paddingLeft: 10,
        paddingRight: 10
    },
    itemContainer: {
        flex: 1,
        flexDirection: 'row',
        borderColor: '#ddd',
        borderTopWidth: 0.5,
        borderBottomWidth: 0.5,
        paddingTop: 10,
        paddingBottom: 10
    },
    itemImageContainer: {
        flex: 1,
        borderRadius: 2,
        borderWidth: 0.5,
        borderColor: '#000'
    },
    itemDetailContainer: {
        flex: 3,
        paddingLeft: 15  
    },
    itemTitle: {

    },
    itemDescription: {
        color: '#808080',
        fontSize: 10
    },
    itemLocation: {
        color: '#808080',
    },
    itemCategory: {
        color: '#808080',
    },
    itemPrice: {
        
    },
    itemImage: {
        flex: 1,
        borderWidth: 2,
        borderRadius: 2,
        borderColor: '#fff',
        shadowColor: 'red',
        shadowOpacity: 0.5,
        shadowOffset: { height: 2, width: 2 },
    }
};


const mapStateToProps = (state) => ({
    nav: state.nav,
    settings: state.settings
});

const mapDispatchToProps = (dispatch) => ({ ...bindActionCreators(ActionCreators, dispatch), dispatch });

export default connect(mapStateToProps, mapDispatchToProps)(MyAdsScreen);
