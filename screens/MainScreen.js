import React, { PureComponent } from 'react';
import { ScrollView, Dimensions, View, AsyncStorage, InteractionManager } from 'react-native';
import ImageSlider from 'react-native-image-slider';
import ParallaxScroll from '@monterosa/react-native-parallax-scroll';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { addNavigationHelpers, NavigationActions } from 'react-navigation';
import ActionButton from 'react-native-action-button';
import axios from 'axios';

import { MainTabNavigator } from '../config/routes';
import MainHeader from '../components/mainHeader';
import { STATUS_BAR_HEIGHT } from '../constants';

import * as ActionCreators from '../actions';

import Spinner from '../components/spinner';
// import Header from './components/header';

class MainScreen extends PureComponent {
    constructor(props) {
        super(props);

        this.state = {
            position: 0,
            interval: null,
            scrollMarginTop: -100,
            showActionButton: true,
            actionButtonText: 'SELL',
            images: [],
            isLoading: true,
            logedIn: false,
            user: null
        };
    }

    componentWillMount() {
        this.setState({ interval: setInterval(() => {
            this.setState({ position: this.state.position === 2 ? 0 : this.state.position + 1 });
        }, 2000) });
    }

    async componentDidMount() {
        try {
            let isLogedIn = await AsyncStorage.getItem('logedIn');
            let user = await AsyncStorage.getItem('user');
    
            isLogedIn = JSON.parse(isLogedIn);
            user = JSON.parse(user)[0];  
            
            this.setState({
                logedIn: isLogedIn,
                user
            });
        } catch (e) {
            this.setState({
                user: null,
                logedIn: false
            });
        } 

        if (this.props.tab.index === 1) {
            this.setState({
                actionButtonText: 'Create Store',
                showActionButton: this.state.logedIn
            });
        } else if (this.props.tab.index === 0) {
            this.setState({
                actionButtonText: 'SELL',
                showActionButton: this.state.logedIn
            });
        } else if (this.props.tab.index === 2) {            
            this.setState({
                showActionButton: false
            });
        }

        if (this.state.images.length === 0) {
            this._getData()
                .then((data) => {
                    if (data) {
                        this.setState({ images: data, isLoading: false });
                    } else {
                        this.setState({ isLoading: false });
                    }
                    
                })
                .catch(err => { throw err; });
        }
    }

    async componentWillReceiveProps(nextProps) {
        if (nextProps.tab.index === 1) {
            this.setState({
                actionButtonText: 'Create Store',
                showActionButton: this.state.logedIn
            });
        } else if (nextProps.tab.index === 0) {
            this.setState({
                actionButtonText: 'SELL',
                showActionButton: this.state.logedIn
            });
        } else if (nextProps.tab.index === 2) {
            this.setState({
                showActionButton: false
            });
        }
    }

    componentWillUnmount() {
        clearInterval(this.state.interval);
    }

    async _getData() {
        try {
            const res = await axios('http://vikka.mv/core/app_banner.php');
            if (!res.data.banners && res.data.banners != null) {
                return res.data.banners.map(obj => `http://www.vikka.mv/images/banners/${obj.banner_app}`);
            }
            return false;
        } catch (e) {
            return false;
        }
        
    }

    _setScroll(status) {
        this.props.setScroll(status);
    }

    _navigateToCreateStore() {
        this.props.dispatch(NavigationActions.navigate({ routeName: 'CreateStore' }));
    }

    _navigateToCreateItem() {
        this.props.dispatch(NavigationActions.navigate({ routeName: 'CreateItem' }));
    }

    _renderActionButton() {
        if (this.state.showActionButton) {
            return (
                <ActionButton
                    position="center"
                    buttonText={this.state.actionButtonText}
                    buttonTextStyle={{ fontSize: 11, marginTop: 1, fontWeight: 'bold', textAlign: 'center' }}
                    buttonColor="red"
                    onPress={() => { (this.state.actionButtonText === 'Create Store') ? this._navigateToCreateStore() : this._navigateToCreateItem(); }}
                />
            );
        }

        return null;
    }

    render() {
        const windowHeight = Dimensions.get('window').height;
        const headerHeight = 50;

        return (
            <View style={{ flex: 1, height: windowHeight }}>
                <ParallaxScroll
                    scrollEventThrottle={16}
                    scrollEnabled={this.props.settings.scrollEnabled}
                    showsVerticalScrollIndicator={false}
                    style={styles.mainContainer}
                    renderHeader={() => <MainHeader />}
                    headerHeight={headerHeight}
                    isHeaderFixed
                    parallaxHeight={50}
                    renderParallaxBackground={() => null}
                    renderParallaxForeground={() => null}
                    parallaxBackgroundScrollSpeed={5}
                    parallaxForegroundScrollSpeed={2.5}
                >

                    {!this.state.isLoading && <ImageSlider
                        images={this.state.images} 
                        position={this.state.position}
                        onPositionChanged={position => this.setState({ position })} 
                    />}

                    {this.state.isLoading && <View style={{ height: 250, justifyContent: 'center' }}>
                        <Spinner
                            size="large"
                            color="red"
                        />
                    </View>}


                    <ScrollView
                        onTouchStart={this._setScroll.bind(this, false)}
                        onMomentumScrollEnd={this._setScroll.bind(this, true)}
                        onScrollEndDrag={this._setScroll.bind(this, true)}
                        scrollEnabled style={{ flexGrow: 1, height: (windowHeight + 25) - (150 - headerHeight) }} 
                        contentContainerStyle={{ flex: 1 }}
                    >
                    
                        
                        <MainTabNavigator
                            navigation={addNavigationHelpers({
                                dispatch: this.props.dispatch,
                                state: this.props.tab,
                            })} 
                        />

                    </ScrollView>
                </ParallaxScroll>

                {this._renderActionButton()}
                
            </View>
        );
    }
}

const styles = {
    mainContainer: {
        flex: 1,
        marginTop: STATUS_BAR_HEIGHT,
        overflow: 'hidden',
        height: 300
    },
    imageContainer: {
        marginTop: -100
    },
};

const mapStateToProps = (state) => ({
    tab: state.tab,
    nav: state.nav,
    settings: state.settings,
});

const mapDispatchToProps = (dispatch) => ({ ...bindActionCreators(ActionCreators, dispatch), dispatch });

export default connect(mapStateToProps, mapDispatchToProps)(MainScreen);
