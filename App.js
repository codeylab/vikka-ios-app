import React, { Component } from 'react';
import { Provider } from 'react-redux';
import Expo from 'expo';


// import Main from './Main';
// import ItemScreen from './screens/ItemScreen';
import configureStore from './store';
import RootNavigator from './RootNavigator';

export default class App extends Component {
  componentDidMount() {
    Expo.ScreenOrientation.allow(Expo.ScreenOrientation.Orientation.PORTRAIT_UP);
  }

  render() {
    const store = configureStore();

    return (
      <Provider store={store}>
          <RootNavigator />
      </Provider>
    );
  }
}
