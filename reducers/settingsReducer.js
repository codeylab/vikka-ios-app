import { Platform } from 'react-native';
import { SET_SCROLL, SET_CURRENT_SCROLL } from '../constants';

const initialState = {
    scrollEnabled: true,
    scrollCurrentOffset: 150
};

export default (state = initialState, action) => {
    switch (action.type) {
    case SET_SCROLL:
      if (Platform.OS === 'android') {
        return { ...state, scrollEnabled: action.payload.status };
      }
      return state;
    
    case SET_CURRENT_SCROLL:
      if (action.payload.offset <= 0) {
        return { ...state, scrollCurrentOffset: 150 };        
      }
      return { ...state, scrollCurrentOffset: action.payload.offset };

    default:
      return state;
  }
};
