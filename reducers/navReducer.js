import { MainNavigator } from '../config/routes';

const initialState = MainNavigator.router.getStateForAction(MainNavigator.router.getActionForPathAndParams('Main'));

export default (state = initialState, action) => {
    const nextState = MainNavigator.router.getStateForAction(action, state);
    // Simply return the original `state` if `nextState` is null or undefined.
    return nextState || state;
};
