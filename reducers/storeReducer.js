import { 
    SET_LOCATION, 
    SHOW_LOCATION_MODAL, 
    SET_CATEGORY, 
    SHOW_CATEGORY_MODAL, 
    SET_STORE, 
    SHOW_STORE_MODAL, 
    SET_SORT, 
    SHOW_SORT_MODAL 
} from '../constants';

const initialState = {
    location: '',
    category: '',
    store: '',
    sort: { name: 'All', value: 1 },
    showLocationModal: false,
    showCategoryModal: false,
    showStoreModal: false,
    showSortModal: false
};

export default (state = initialState, action) => {
    switch (action.type) {
        case SET_LOCATION:
            return { ...state, location: action.payload.location };

        case SHOW_LOCATION_MODAL:
            return { ...state, showLocationModal: action.payload.show };

        case SET_CATEGORY:
            return { ...state, category: action.payload.category };

        case SHOW_CATEGORY_MODAL:
            return { ...state, showCategoryModal: action.payload.show };

        case SET_STORE:
            return { ...state, store: action.payload.store };

        case SHOW_STORE_MODAL:
            return { ...state, showStoreModal: action.payload.show };

        case SET_SORT:
            return { ...state, sort: action.payload.sort };

        case SHOW_SORT_MODAL:
            return { ...state, showSortModal: action.payload.show };

        default:
            return state;
  }
};
