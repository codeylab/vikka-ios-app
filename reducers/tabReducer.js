import { MainTabNavigator } from '../config/routes';

const initialState = MainTabNavigator.router.getStateForAction(MainTabNavigator.router.getActionForPathAndParams('Home'));

export default (state = initialState, action) => {
    const nextState = MainTabNavigator.router.getStateForAction(action, state);
    // Simply return the original `state` if `nextState` is null or undefined.
    return nextState || state;
};
