import { combineReducers } from 'redux';
import settingsReducer from './settingsReducer';
import navReducer from './navReducer';
import tabReducer from './tabReducer';
import storeReducer from './storeReducer';

export default combineReducers({
    nav: navReducer,   
    tab: tabReducer,
    settings: settingsReducer,
    store: storeReducer
});
